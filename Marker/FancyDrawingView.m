//
//  FancyDrawingView.m
//  Sessions
//
//  Created by developer on 8/29/15.
//  Copyright © 2015 TouchCast. All rights reserved.
//

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import "FancyDrawingView.h"
#import <UIKit/UIGestureRecognizerSubclass.h>
#import "MarkerTouch.h"
#import "GPUImage.h"

@import GLKit;

#define MAX_BUFFERED_POINTS         100000
#define MAX_PREDICTED_POINTS        100

#define BRUSH_PIXEL_STEP            1.0
#define DRAWING_SPEED_PX_PER_SEC    500

#define MIN_LINE_WIDTH              10
#define MAX_LINE_WIDTH              48

#define DEFAULT_FORCE               2.0f  
#define DEFAULT_MAX_FORCE           4.6f

#define NUM_POINTS_TO_APPROXIMATE   10
#define APPROXIMATION_STEP          100

#define DELAY                       100 //in miliseconds
//#define DRAW_PREDICTION

typedef enum
{
    kDrawingProgramUniformProjectionMatrix = 0,
    kDrawingProgramUniformPointColor,
    kDrawingProgramUniformTextureName,
    kDrawingProgramUniformSingeBrushSizeInAtlas,
    kDrawingProgramUniformMax
    
} DrawingProgramUniform;

typedef enum
{
    kDrawingProgramAttributeVertexes = 0,
    kDrawingProgramAttributeBrushIndex, //index of the brush image in the atlas: vec2 with horizontal and vertical item indexes
    kDrawingProgramAttributeBrushWidth, //brush width in pixels
    kDrawingProgramAttributeMax
    
} DrawingProgramAttribute;

typedef enum
{
    kTexturePresentingProgramAttributeVertexes = 0,
    kTexturePresentingProgramAttributeTextureCoords,
    kTexturePresentingProgramAttributeMax
} TexturePresentingProgramAttribute;

typedef enum
{
    kTexturePresentingProgramUniformProjectionMatrix = 0,
    kTexturePresentingProgramUniformTextureName,
    kTexturePresentingProgramUniformMax,
} TexturePresentingProgramUniform;

typedef struct
{
    GLuint  name;
    GLsizei width, height;
    
} FancyDrawingViewTextureInfo;


typedef struct
{
     FancyDrawingViewTextureInfo texture;
     GLfloat                     textureAtlasParams[2];
    
} FancyDrawingViewBrushInfo;


@interface FancyDrawingViewPointsSet : NSObject

@property(readonly) NSUInteger  capacity;

//list of points. 2 * i for x, 2 * i + 1 for y
@property(readonly) GLfloat*    points;

@property(readonly) GLfloat*    brushIndexes;
@property(readonly) GLfloat*    widths;
@property(readonly) GLfloat*    opacities;

@property(assign)   NSUInteger  size;

- (instancetype)initWithCapacity:(NSUInteger)capacity;

@end

@interface FancyDrawingView()
{    
    CGSize              _pixelsSize;
    float               _scale;
    
    //brush info
    GLfloat             _brushColor[4];
    
    FancyDrawingViewBrushInfo   _brushes[FancyDrawingViewBrushTypeMax];
    FancyDrawingViewBrushType   _currentBrushType;
    
    
    //will draw on the view using display link
    //if canvas is dirty
    CADisplayLink*              _displayLink;
    
    FancyDrawingViewPointsSet*   _bufferedPoints;
    FancyDrawingViewPointsSet*   _predictedPoints;
    NSMutableArray<NSValue*>*    _pointsForPrediction;
    
    NSMutableArray<MarkerTouch*>* _touchesForPrediction;
    
    //hack used how low pass filter
    float                       _lastInterpolatedLineWidth;
    
    //offscreen drawing
    //used to store already approved touches. Predicted will be drawn on top
    GPUImageFilter*     _backgroundFilter;

    GLKMatrix4          _combinedProjectionModelViewMatrix;
    
    GLProgram*          _drawingProgram;
    GLuint              _drawingProgramAttributes[kDrawingProgramAttributeMax];
    GLuint              _drawingProgramUniforms[kDrawingProgramUniformMax];
    
    //onscreen drawing
    GLProgram*          _texturePresentingProgram;
    GLuint              _texturePresentingProgramAttributes[kTexturePresentingProgramAttributeMax];
    GLuint              _texturePresentingProgramUniforms[kTexturePresentingProgramUniformMax];
    
    GLuint              _displayRenderbuffer;
    GLuint              _displayFramebuffer;
}


@end

@implementation FancyDrawingView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

+ (Class)layerClass
{
    return [CAEAGLLayer class];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame: frame])
    {
        self.backgroundColor = [UIColor clearColor];
        self.opaque = NO;
        
        //touches will be transfered from different view
        self.userInteractionEnabled = NO;

        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
        eaglLayer.opaque = NO;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],
                                        kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        _lastInterpolatedLineWidth = MIN_LINE_WIDTH;
        
        _bufferedPoints = [[FancyDrawingViewPointsSet alloc] initWithCapacity: MAX_BUFFERED_POINTS];
        _predictedPoints = [[FancyDrawingViewPointsSet alloc] initWithCapacity: MAX_PREDICTED_POINTS];
    
        _touchesForPrediction = [NSMutableArray new];
        _pointsForPrediction = [NSMutableArray new];
        
        _displayLink = [CADisplayLink displayLinkWithTarget: self
                                                   selector: @selector(onDisplayLinkFire)];
//        _displayLink.paused = YES;
        [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
        
        //setting default brush color
        _brushColor[0] = 0.0f;
        _brushColor[1] = 0.0f;
        _brushColor[2] = 1.0f;
        _brushColor[3] = 1.0f;
        
        runSynchronouslyOnVideoProcessingQueue(^
        {
            [GPUImageOpenGLESContext useImageProcessingContext];
            
            _backgroundFilter = [[GPUImageFilter alloc] initWithVertexShaderFromString: kGPUImageVertexShaderString
                                                              fragmentShaderFromString: kGPUImagePassthroughFragmentShaderString];
            
            if(![self loadDrawingProgram])
            {
                NSAssert(0, @"Could not load Drawing Program");
                return;
            }
            
            if(![self loadPresentingProgram])
            {
                NSAssert(0, @"Could not load Presentation Program");
                return;
            }
            
            //FIXME: for now only pen will work
            //we need somehow to make brush separate class
            //ink pen
            {
                _brushes[FancyDrawingViewBrushTypeInkPen].textureAtlasParams[0] = 1.0f;
                _brushes[FancyDrawingViewBrushTypeInkPen].textureAtlasParams[1] = 1.0f;
                
                _brushes[FancyDrawingViewBrushTypeInkPen].texture = [self loadTextureFromUIImage: [UIImage imageNamed: @"PenBrushAtlas"]];
            }
            
            _scale = [UIScreen mainScreen].scale;
            _pixelsSize = self.bounds.size;
            _pixelsSize.width *= _scale;
            _pixelsSize.height *= _scale;
            
            //update model-view and projection matrix
            // Update projection matrix
            GLKMatrix4 projectionMatrix = GLKMatrix4MakeOrtho(0,
                                                              _pixelsSize.width,
                                                              _pixelsSize.height,
                                                              0,
                                                              -1,
                                                              1);
            GLKMatrix4 modelViewMatrix = GLKMatrix4Identity; // this sample uses a constant identity modelView matrix
            _combinedProjectionModelViewMatrix = GLKMatrix4Multiply(projectionMatrix, modelViewMatrix);
            
            [_backgroundFilter forceProcessingAtSize: _pixelsSize];
            [self clear];
            
            //setup new rendering buffers
//            runSynchronouslyOnVideoProcessingQueue(
//                                                   ^{
//                                                       [self destroyDisplayFramebuffer];
//                                                       [self createDisplayFramebuffer];
//                                                   });
            
        });
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    

}

- (void)willMoveToWindow:(UIWindow *)newWindow
{
    if(newWindow)
    {

        _displayLink.paused = NO;
        
    }
    else
    {
        _displayLink.paused = YES;
    }
}

#pragma mark Public calls

- (void)setBrushColorWithRed:(CGFloat)red
                       green:(CGFloat)green
                        blue:(CGFloat)blue
{
    _brushColor[0] = red;
    _brushColor[1] = green;
    _brushColor[2] = blue;
    _brushColor[3] = 1.0f;
}

- (void)clear
{
    runSynchronouslyOnVideoProcessingQueue(^
   {
       [GPUImageOpenGLESContext useImageProcessingContext];

       //cleaning offscreen texture
       [_backgroundFilter setFilterFBO];
      
       [self clearGL];
       
       //drawing empty
       [self setDisplayFramebuffer];
       [self presentFramebuffer];
   });
}

- (UIImage*)drawing
{
    __block UIImage* result = nil;
    runSynchronouslyOnVideoProcessingQueue(^
    {
        [GPUImageOpenGLESContext useImageProcessingContext];
        //[self movePredictedTouchcesToRealTouches];
        [self updateCanvas];
        glFinish();
       
        CGSize imageSize = CGSizeMake(self.bounds.size.width,
                                      self.bounds.size.height);
        
        UIGraphicsBeginImageContextWithOptions(imageSize, NO, [UIScreen mainScreen].scale);
       
        [self drawViewHierarchyInRect:self.bounds afterScreenUpdates: YES];
        
        result = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
   });
    
    return result;
}

- (void)processMarkerTouch:(MarkerTouch*)touch
{
    runSynchronouslyOnVideoProcessingQueue(^
                                           {
    // _predictedPoints.size = 0;
    CGFloat r, g, b, a = 0;
    [touch.color getRed:&r green:&g blue:&b alpha:&a];
    
    _brushColor[0] = r;
    _brushColor[1] = g;
    _brushColor[2] = b;
    _brushColor[3] = a;
    
    [self interpolatePointsFrom:touch.prevPosition
                             to:touch.currentPosition
                          force:DEFAULT_FORCE
                       maxForce:DEFAULT_FORCE
                    inPointsSet:_bufferedPoints
         bypassWidthCalculation:NO];
    
    if (_touchesForPrediction.count == 0) {
        [_touchesForPrediction addObject:touch];
    }
    else if (_touchesForPrediction.count == NUM_POINTS_TO_APPROXIMATE) {
        [_touchesForPrediction removeObjectAtIndex:0];
        
    }
    
    if (!CGPointEqualToPoint(((MarkerTouch *)[_touchesForPrediction lastObject]).currentPosition, touch.currentPosition)) {
        [_touchesForPrediction addObject:touch];
    }
    
    //[self calculatePredictedTouches];
    _predictedPoints.size = 0;
    [self calculateTouch];
    //_predictedPoints.size = 0;
                                           });
}

- (void)processMarkerPoints:(NSArray<NSValue *> *)points
{
    if(points.count < 2)
    {
        return;
    }
    
//    GLfloat force = DEFAULT_FORCE;
//    GLfloat maxForce = DEFAULT_MAX_FORCE;
    
    [self interpolatePointsFrom: [points[0] CGPointValue]
                             to: [points[1] CGPointValue]
                          force: DEFAULT_FORCE
                       maxForce: DEFAULT_FORCE
                    inPointsSet: _bufferedPoints
         bypassWidthCalculation: NO];
    
    if(_pointsForPrediction.count == 0)
    {
        [_pointsForPrediction addObject:[NSValue valueWithCGPoint:[points[0] CGPointValue]]];
    }
    else if(_pointsForPrediction.count == NUM_POINTS_TO_APPROXIMATE)
    {
        [_pointsForPrediction removeObjectAtIndex:0];
    }
    
    if(!CGPointEqualToPoint([[_pointsForPrediction lastObject] CGPointValue], [points[1] CGPointValue]))
    {
//        NSLog(@"old %@", NSStringFromCGPoint([[_pointsForPrediction lastObject] CGPointValue]));
//        NSLog(@"new %@", NSStringFromCGPoint([points[1] CGPointValue]));
        [_pointsForPrediction addObject:[NSValue valueWithCGPoint:[points[1] CGPointValue]]];
    }
    
    [self calculatePredictedPoints];
}

-(void)clearPointsHistory
{
    //[_pointsForPrediction removeAllObjects];
    //[_touchesForPrediction removeAllObjects];
    _predictedPoints.size = 0;
}

#pragma mark - Private Calls

#pragma mark Prediction calculation

- (int) signum: (float) n
{ return (n < 0) ? -1 : (n > 0) ? +1 : 0;
}

- (void) calculateTouch
{
    if (_touchesForPrediction.count == 0 /*!= NUM_POINTS_TO_APPROXIMATE*/)
    {
        NSLog(@"Not enough touches");
        return;
    }
    NSInteger countToAproximate = NUM_POINTS_TO_APPROXIMATE;
    CGFloat velocityX = 0;
    CGFloat velocityY = 0;
    CGFloat velocity = 0;
    if (_touchesForPrediction.count > 1)
    {
        if (_touchesForPrediction.count < countToAproximate)
        {
            countToAproximate = _touchesForPrediction.count;
        }
    
        NSDate* prevTime = [_touchesForPrediction firstObject].timestamp;
        for (int i = 1; i< countToAproximate; i++)
        {
            NSTimeInterval secondsBetween = [_touchesForPrediction[i].timestamp timeIntervalSinceDate: prevTime];
            
            CGFloat dist = sqrt(pow((_touchesForPrediction[i].currentPosition.x - _touchesForPrediction[i].prevPosition.x), 2.0) + pow((_touchesForPrediction[i].currentPosition.y - _touchesForPrediction[i].prevPosition.y), 2.0));
            velocity =  velocity*0.9 + 0.1*dist/secondsBetween/1000;
        
            velocityX += (_touchesForPrediction[i].currentPosition.x - _touchesForPrediction[i].prevPosition.x)/secondsBetween;
            velocityY += (_touchesForPrediction[i].currentPosition.y - _touchesForPrediction[i].prevPosition.y)/secondsBetween;//*1.0f;
            prevTime = _touchesForPrediction[i].timestamp;
        
        
        }
        velocity = velocity / countToAproximate;
        velocityX = velocityX / countToAproximate/1000;
        velocityY = velocityY / countToAproximate/1000;
    }
    //CGFloat resVelocity = sqrt(pow(velocityX,2)+ pow(velocityY, 2));
    //create vector of direction
    CGPoint vector = CGPointMake([_touchesForPrediction lastObject].currentPosition.x - [_touchesForPrediction lastObject].prevPosition.x, [_touchesForPrediction lastObject].currentPosition.y -[_touchesForPrediction lastObject].prevPosition.y);
    CGFloat length = sqrt(pow(vector.x,2.0)+pow(vector.y, 2.0));
    //normalize vector of direction
    vector.x = vector.x/length;
    vector.y = vector.y/length;
//    NSLog(@"velocity: %f", velocity);
    CGPoint previousTouchPoint = [_touchesForPrediction lastObject].currentPosition;

    //
    CGFloat predictedX = [_touchesForPrediction lastObject].currentPosition.x + vector.x*velocity*DELAY;
    CGFloat predictedY = [_touchesForPrediction lastObject].currentPosition.y + vector.y*velocity*DELAY;
//    NSLog(@"last %@", NSStringFromCGPoint(previousTouchPoint));
//    NSLog(@"predicted %@", NSStringFromCGPoint(CGPointMake(predictedX, predictedY)));
  
    [self interpolatePointsFrom: previousTouchPoint
                             to: CGPointMake(predictedX, predictedY)
                          force: DEFAULT_FORCE
                       maxForce: DEFAULT_FORCE
                    inPointsSet: _predictedPoints
         bypassWidthCalculation: NO];
    previousTouchPoint = CGPointMake(predictedX, predictedY);

}
- (void) calculatePredictedTouches
{
    if (_touchesForPrediction.count != NUM_POINTS_TO_APPROXIMATE)
    {
         //NSLog(@"Not enough touches");
        return;
    }
   
    NSMutableArray* points = [[NSMutableArray alloc] init];
    [points addObject:[NSValue valueWithCGPoint:[_touchesForPrediction firstObject].prevPosition]];
    for (int i = 0; i< _touchesForPrediction.count; i++)
    {
        [points addObject:[NSValue valueWithCGPoint:_touchesForPrediction[i].currentPosition]];
    }
    
    
    CGFloat velocityX = 0;
    NSDate* prevTime = [_touchesForPrediction firstObject].timestamp;
    for (int i = 1; i< _touchesForPrediction.count; i++)
    {
        NSTimeInterval secondsBetween = [_touchesForPrediction[i].timestamp timeIntervalSinceDate: prevTime];
        // NSLog(@"sec: %f", secondsBetween);

        velocityX += (_touchesForPrediction[i].currentPosition.x - _touchesForPrediction[i].prevPosition.x)/secondsBetween;//*1.0f;
        prevTime = _touchesForPrediction[i].timestamp;
        
        
    }
    velocityX = velocityX / NUM_POINTS_TO_APPROXIMATE/1000;
//    NSLog(@"velocity: %f", velocityX);
//    NSLog(@"___________");
//    for (MarkerTouch* pValue in _touchesForPrediction)
//    {
//        NSLog(@"Previous: %@", NSStringFromCGPoint(pValue.prevPosition));
//        NSLog(@"Current: %@", NSStringFromCGPoint(pValue.currentPosition));
//    }
//    NSLog(@"___________");
    CGPoint previousTouchPoint = [_touchesForPrediction lastObject].currentPosition;
 
    CGFloat predictedX = previousTouchPoint.x + /*([_touchesForPrediction lastObject].currentPosition.x - [_touchesForPrediction lastObject].prevPosition.x)+*/velocityX*10;
 
    // --adding
 
  //  NSLog(@"==predicted X: %f, prev X: %f==", predictedX, previousTouchPoint.x);
    
    
    CGFloat predictedY = 0;
//    for (int i = 0; i < NUM_POINTS_TO_APPROXIMATE; i++)
//    {
//        //            NSLog(@"%d. %@", i, NSStringFromCGPoint([_pointsForPrediction[i] CGPointValue]));
//        CGFloat numerator = 1;
//        CGFloat denominator = 1;
//        for (int j = 0; j < NUM_POINTS_TO_APPROXIMATE; j++)
//        {
//            if(j == i)
//            {
//                continue;
//            }
//            numerator *= predictedX - _touchesForPrediction[j].prevPosition.x;
//            denominator *= _touchesForPrediction[i].prevPosition.x - _touchesForPrediction[j].prevPosition.x;
//            
//            
//        }
//        
//        predictedY += (numerator / denominator) * _touchesForPrediction[i].prevPosition.y;
//        if (denominator == 0)
//        {
//       //     NSLog(@"denominator is 0");
//            predictedX = previousTouchPoint.x;
//            predictedY = previousTouchPoint.y;
//            
//            break;
//        }
//    }
    for (int i = 0; i < points.count; i++)
    {
        //            NSLog(@"%d. %@", i, NSStringFromCGPoint([_pointsForPrediction[i] CGPointValue]));
        CGFloat numerator = 1;
        CGFloat denominator = 1;
        for (int j = 0; j < points.count; j++)
        {
            if(j == i)
            {
                continue;
            }
            numerator *= predictedX - [points[j] CGPointValue].x;
            denominator *= [points[i] CGPointValue].x - [points[j] CGPointValue].x;
            
            
        }
        predictedY += (numerator / denominator) * [points[i] CGPointValue].y;
    }
    //other way with vectors
    CGPoint prevVect = CGPointMake([_touchesForPrediction firstObject].currentPosition.x - [_touchesForPrediction firstObject].prevPosition.x, [_touchesForPrediction firstObject].currentPosition.y - [_touchesForPrediction firstObject].prevPosition.y);
    CGFloat resAngle = 0;
    for (int i = 1; i < NUM_POINTS_TO_APPROXIMATE; i++)
    {
           CGPoint currVect = CGPointMake(_touchesForPrediction[i].currentPosition.x - _touchesForPrediction[i].prevPosition.x, _touchesForPrediction[i].currentPosition.y - _touchesForPrediction[i].prevPosition.y);
        CGFloat scalar = prevVect.x*currVect.x + prevVect.y*currVect.y;
        CGFloat moduleVectPrev = sqrt(pow(prevVect.x,2)+pow(prevVect.y,2));
        CGFloat moduleVectCurr = sqrt(pow(currVect.x,2)+pow(currVect.y,2));
        
        CGFloat angle = acos(scalar/(moduleVectCurr*moduleVectPrev));
       // NSLog(@"angle: %f", angle);
                             
        
        resAngle = resAngle + angle;
        prevVect = currVect;
        
    }
    resAngle = resAngle / NUM_POINTS_TO_APPROXIMATE;
    CGPoint resultVect = CGPointMake(predictedX -[_touchesForPrediction lastObject].currentPosition.x, predictedX -[_touchesForPrediction lastObject].currentPosition.x);
    predictedX = [_touchesForPrediction lastObject].currentPosition.x + resultVect.x*cos(resAngle);
    predictedY = [_touchesForPrediction lastObject].currentPosition.y + resultVect.y*sin(resAngle);
    
    
    //adding ended
     NSLog(@"last %@", NSStringFromCGPoint(previousTouchPoint));
    NSLog(@"predicted %@", NSStringFromCGPoint(CGPointMake(predictedX, predictedY)));
    [self interpolatePointsFrom: previousTouchPoint
                             to: CGPointMake(predictedX, predictedY)
                          force: DEFAULT_FORCE
                       maxForce: DEFAULT_FORCE
                    inPointsSet: _predictedPoints
         bypassWidthCalculation: NO];
    previousTouchPoint = CGPointMake(predictedX, predictedY);
    //    }
}

- (void) calculatePredictedPoints
{
    if(_pointsForPrediction.count != NUM_POINTS_TO_APPROXIMATE)
    {
        return;
    }
    NSLog(@"___________");
    for (NSValue* pValue in _pointsForPrediction)
    {
        NSLog(@"%@", NSStringFromCGPoint([pValue CGPointValue]));
    }
    NSLog(@"___________");
    
    CGPoint previousTouchPoint = [[_pointsForPrediction lastObject] CGPointValue];
    //    NSLog(@"_________________prediction____________________");
    //    for (int k = 0; k < NUM_POINTS_TO_APPROXIMATE; k++)
    //    {
    CGFloat predictedX = previousTouchPoint.x + ([[_pointsForPrediction lastObject] CGPointValue].x - [_pointsForPrediction[NUM_POINTS_TO_APPROXIMATE - 2] CGPointValue].x);//previousTouchPoint.x + APPROXIMATION_STEP * (k + 1);
    // --adding
    CGFloat midX = 0;
    CGFloat acosY = 0;
    //    for (int j = 1; j < NUM_POINTS_TO_APPROXIMATE-1; j++)
    //    {
    //        CGPoint vec1 = CGPointMake( [_pointsForPrediction[j-1] CGPointValue].x - [_pointsForPrediction[j] CGPointValue].x, [_pointsForPrediction[j-1] CGPointValue].y - [_pointsForPrediction[j] CGPointValue].y);
    //        CGPoint vec2 = CGPointMake( [_pointsForPrediction[j] CGPointValue].x - [_pointsForPrediction[j+1] CGPointValue].x, [_pointsForPrediction[j] CGPointValue].y - [_pointsForPrediction[j+1] CGPointValue].y);
    //        CGFloat scalar = vec1.x * vec2.x + vec1.y * vec2.y;
    //
    //        CGFloat module1 = sqrt(pow(vec1.x,2.0)+pow(vec1.y,2.0));
    //        CGFloat module2 = sqrt(pow(vec2.x,2.0)+pow(vec2.y,2.0));
    //
    //        acosY = acos(scalar/(module1*module2));
    //
    //    }
    //    acosY = acosY / NUM_POINTS_TO_APPROXIMATE;
    
    for (int i=1; i< NUM_POINTS_TO_APPROXIMATE; i++)
    {
        midX += ([_pointsForPrediction[i] CGPointValue].x - [_pointsForPrediction[i-1] CGPointValue].x);
    }
    midX = midX / (NUM_POINTS_TO_APPROXIMATE -1);
    //predictedX = previousTouchPoint.x + midX;
    // --adding ended
    NSLog(@"==predicted X: %f, prev X: %f==", predictedX, previousTouchPoint.x);
    
    
    CGFloat predictedY = 0;
    for (int i = 0; i < NUM_POINTS_TO_APPROXIMATE; i++)
    {
        //            NSLog(@"%d. %@", i, NSStringFromCGPoint([_pointsForPrediction[i] CGPointValue]));
        CGFloat numerator = 1;
        CGFloat denominator = 1;
        for (int j = 0; j < NUM_POINTS_TO_APPROXIMATE; j++)
        {
            if(j == i)
            {
                continue;
            }
            numerator *= predictedX - [_pointsForPrediction[j] CGPointValue].x;
            denominator *= [_pointsForPrediction[i] CGPointValue].x - [_pointsForPrediction[j] CGPointValue].x;
            
            
        }
        //NSLog(@"numerator: %f", numerator);
        //NSLog(@"denominator: %f", denominator);
        predictedY += (numerator / denominator) * [_pointsForPrediction[i] CGPointValue].y;
//        if (denominator == 0)
//        {
//            predictedX = previousTouchPoint.x;
//            predictedY = previousTouchPoint.y;
//            
//            break;
//        }
    }
    //adding
//    if (predictedY == INFINITY || predictedY == NAN || predictedY == -INFINITY)
//    {
//        predictedX = previousTouchPoint.x;
//        predictedY = previousTouchPoint.y;
//    }
    
    // predictedY = predictedY*cos(acosY) - predictedX*sin(acosY);
    
    //adding ended
    NSLog(@"predicted %@", NSStringFromCGPoint(CGPointMake(predictedX, predictedY)));
    [self interpolatePointsFrom: previousTouchPoint
                             to: CGPointMake(predictedX, predictedY)
                          force: DEFAULT_FORCE
                       maxForce: DEFAULT_FORCE
                    inPointsSet: _predictedPoints
         bypassWidthCalculation: NO];
    previousTouchPoint = CGPointMake(predictedX, predictedY);
    //    }
    
}

// Newton Polynomial
- (void) calculatePredictedPointsNewton
{
    if(_pointsForPrediction.count != NUM_POINTS_TO_APPROXIMATE)
    {
        return;
    }
    NSLog(@"___________");
    for (NSValue* pValue in _pointsForPrediction)
    {
        NSLog(@"%@", NSStringFromCGPoint([pValue CGPointValue]));
    }
    NSLog(@"___________");
    
    CGPoint previousTouchPoint = [[_pointsForPrediction lastObject] CGPointValue];
 
    CGFloat predictedX = previousTouchPoint.x + ([[_pointsForPrediction lastObject] CGPointValue].x - [_pointsForPrediction[NUM_POINTS_TO_APPROXIMATE - 2] CGPointValue].x);
    CGFloat predictedY = 0;
    CGFloat xFirst = [[_pointsForPrediction firstObject] CGPointValue].x;
    CGFloat xLast = [[_pointsForPrediction lastObject] CGPointValue].x;
    CGFloat aFirst = 0;
    CGFloat aLast = 0;
    CGFloat yFirst = 0;
    CGFloat yLast = 0;
    
    aFirst = [[_pointsForPrediction firstObject] CGPointValue].y;
    yFirst =[[_pointsForPrediction firstObject] CGPointValue].y;
    yLast = [_pointsForPrediction[1] CGPointValue].y;
    aLast = (yLast - aFirst)/(xLast - xFirst);

    predictedY = aFirst + aLast*(predictedX-xFirst);
    yLast = aLast;
    for (int i = 2; i < NUM_POINTS_TO_APPROXIMATE; i++)
        {
            yFirst = yLast;
            yLast = ([_pointsForPrediction[i] CGPointValue].y - [_pointsForPrediction[i-1] CGPointValue].y)/([_pointsForPrediction[i] CGPointValue].x - [_pointsForPrediction[i-1] CGPointValue].x);
            aLast = (yLast - yFirst)/([_pointsForPrediction[i] CGPointValue].x - [_pointsForPrediction[i-2] CGPointValue].x);
            CGFloat multiply = 1;
            for (int j = 0; j <= i; j++)
            {
                multiply *= (predictedX - [_pointsForPrediction[j] CGPointValue].x);
            }
            predictedY += aLast*multiply;
            
        }
    NSLog(@"predicted %@", NSStringFromCGPoint(CGPointMake(predictedX, predictedY)));
    [self interpolatePointsFrom: previousTouchPoint
                             to: CGPointMake(predictedX, predictedY)
                          force: DEFAULT_FORCE
                       maxForce: DEFAULT_FORCE
                    inPointsSet: _predictedPoints
         bypassWidthCalculation: NO];
    previousTouchPoint = CGPointMake(predictedX, predictedY);
    //    }
    
}

// Second Newton Polynomial
- (void) calculatePredictedPointsNewtonSecond
{
    if(_pointsForPrediction.count != NUM_POINTS_TO_APPROXIMATE)
    {
        return;
    }
    NSLog(@"___________");
    for (NSValue* pValue in _pointsForPrediction)
    {
        NSLog(@"%@", NSStringFromCGPoint([pValue CGPointValue]));
    }
    NSLog(@"___________");
    
    CGPoint previousTouchPoint = [[_pointsForPrediction lastObject] CGPointValue];
    
    CGFloat predictedX = previousTouchPoint.x + ([[_pointsForPrediction lastObject] CGPointValue].x - [_pointsForPrediction[NUM_POINTS_TO_APPROXIMATE - 2] CGPointValue].x);
    CGFloat predictedY = 0;
    CGFloat xFirst = [[_pointsForPrediction firstObject] CGPointValue].x;
    CGFloat xLast = [[_pointsForPrediction lastObject] CGPointValue].x;
    CGFloat aFirst = 0;
    CGFloat aLast = 0;
    CGFloat yFirst = 0;
    CGFloat yLast = 0;
    
    aFirst = [[_pointsForPrediction firstObject] CGPointValue].y;
    yFirst =[[_pointsForPrediction firstObject] CGPointValue].y;
    yLast = [_pointsForPrediction[1] CGPointValue].y;
    aLast = (yLast - aFirst)/(xLast - xFirst);
    
    predictedY = aFirst + aLast*(predictedX-xFirst)/(xLast-xFirst);
    yLast = aLast;
    for (int i = 2; i < NUM_POINTS_TO_APPROXIMATE; i++)
    {
        yFirst = yLast;
        yLast = ([_pointsForPrediction[i] CGPointValue].y - [_pointsForPrediction[i-1] CGPointValue].y)/([_pointsForPrediction[i] CGPointValue].x - [_pointsForPrediction[i-1] CGPointValue].x);
        aLast = (yLast - yFirst)/([_pointsForPrediction[i] CGPointValue].x - [_pointsForPrediction[i-2] CGPointValue].x);
        CGFloat multiply = 1;
        CGFloat factorial = 1;
        CGFloat divider = ([_pointsForPrediction[i] CGPointValue].x - [_pointsForPrediction[i-1] CGPointValue].x);
        for (int j = 0; j <= i; j++)
        {
            multiply *= (predictedX - [_pointsForPrediction[j] CGPointValue].x);
            if (j!=0)
            {
                factorial *= j;
            }
        }
        predictedY += pow(aLast, i)*multiply / (factorial * pow(divider, i));
        
    }
    NSLog(@"predicted %@", NSStringFromCGPoint(CGPointMake(predictedX, predictedY)));
    [self interpolatePointsFrom: previousTouchPoint
                             to: CGPointMake(predictedX, predictedY)
                          force: DEFAULT_FORCE
                       maxForce: DEFAULT_FORCE
                    inPointsSet: _predictedPoints
         bypassWidthCalculation: NO];
    previousTouchPoint = CGPointMake(predictedX, predictedY);
    //    }
    
}
// Gregory-Newton Difference
- (void) calculatePredictedPointsNewtonGregory
{
    if(_pointsForPrediction.count != NUM_POINTS_TO_APPROXIMATE)
    {
        return;
    }
    NSLog(@"___________");
    for (NSValue* pValue in _pointsForPrediction)
    {
        NSLog(@"%@", NSStringFromCGPoint([pValue CGPointValue]));
    }
    NSLog(@"___________");
    
    CGPoint previousTouchPoint = [[_pointsForPrediction lastObject] CGPointValue];
    
    CGFloat predictedX = previousTouchPoint.x + ([[_pointsForPrediction lastObject] CGPointValue].x - [_pointsForPrediction[NUM_POINTS_TO_APPROXIMATE - 2] CGPointValue].x);
    CGFloat predictedY = 0;
    CGFloat h = ([[_pointsForPrediction lastObject] CGPointValue].x - [_pointsForPrediction[NUM_POINTS_TO_APPROXIMATE - 2] CGPointValue].x); ////maybe speed calculated interval
    CGFloat s = (predictedX - [[_pointsForPrediction firstObject] CGPointValue].x)/h;
    
    NSMutableArray* yArr = [[NSMutableArray  alloc] init];
    for (int i = 0; i < NUM_POINTS_TO_APPROXIMATE; i++)
    {
        [yArr addObject: @([_pointsForPrediction[i] CGPointValue].y)];
    }
    NSMutableArray* diffArr = [[NSMutableArray  alloc] init];
    [self makeDifferenceTable:yArr result:diffArr];
  //  int countOfTable = diffArr.count;
    for (int j = 0; j< diffArr.count; j++)
    {
        CGFloat factorial = 1;
        CGFloat multiplyer = s;
        if (j != 0)
        {
            for (int i = 1; i<=j; i++)
            {
                factorial *= i;
                multiplyer *= (s-i);
                
            }
        }
        multiplyer = multiplyer /factorial;
        NSMutableArray* array = [diffArr objectAtIndex:j];
        predictedY += multiplyer * [[array objectAtIndex:0] floatValue];
    }
    //
    NSLog(@"predicted %@", NSStringFromCGPoint(CGPointMake(predictedX, predictedY)));
    [self interpolatePointsFrom: previousTouchPoint
                             to: CGPointMake(predictedX, predictedY)
                          force: DEFAULT_FORCE
                       maxForce: DEFAULT_FORCE
                    inPointsSet: _predictedPoints
         bypassWidthCalculation: NO];
    previousTouchPoint = CGPointMake(predictedX, predictedY);
    //    }
    
}
- (void) makeDifferenceTable: (NSMutableArray*) array result: (NSMutableArray*) res
{
    NSMutableArray* currentDifferences = [[NSMutableArray alloc] init];
    for (int i = 1; i < array.count; i++)
    {
        CGFloat diff = [[array objectAtIndex:i] floatValue] - [[array objectAtIndex:i-1] floatValue];
        [currentDifferences addObject:@(diff)];
        
    }
    [res addObject:currentDifferences];
    if (currentDifferences.count>1)
    {
        [self makeDifferenceTable:currentDifferences result:res];
    }
    else
    {
        return;
    }
}
#pragma mark Drawing Calss

- (void)onDisplayLinkFire
{
    runSynchronouslyOnVideoProcessingQueue(^
                                           {
                                               [self updateCanvas];
                                           });
}

- (void)clearGL
{
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}

//this method should be called in the VideoProcessingThread
- (void)updateCanvas
{
    [GPUImageOpenGLESContext useImageProcessingContext];
    
    //setting otput to background texture
    [_backgroundFilter setFilterFBO];

    //setup gl
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
    //[self setDisplayFramebuffer];
    //draw touchces here
//    [self setBrushColorWithRed:1 green:0 blue:0];
    [self drawPointsSet: _bufferedPoints];
    _bufferedPoints.size = 0;
    
    //switching to the onscreen buffer
   [self setDisplayFramebuffer];
    
    //drawing offscreen texture
    [self drawTexture: _backgroundFilter.textureForOutput];
    [self setBrushColorWithRed:0 green:0 blue:1];
    //draw predicted touches here
#ifdef DRAW_PREDICTION
    [self drawPointsSet: _predictedPoints];
#endif
    //_predictedPoints.size = 0;
    //presenting buffer on screen
    [self presentFramebuffer];

    //cleanup
    glDisable(GL_BLEND);
}

//method should be called from the video processing thread
//fl should be setup
- (void)drawPointsSet:(FancyDrawingViewPointsSet*)pointsSet
{
    if(!pointsSet.size)
    {
        return;
    }
    
    [GPUImageOpenGLESContext setActiveShaderProgram: _drawingProgram];
    
    //setting up attributes
    glEnableVertexAttribArray(_drawingProgramAttributes[kDrawingProgramAttributeVertexes]);
    glVertexAttribPointer(_drawingProgramAttributes[kDrawingProgramAttributeVertexes],
                          2,
                          GL_FLOAT,
                          0,
                          0,
                          pointsSet.points);
    
    glEnableVertexAttribArray(_drawingProgramAttributes[kDrawingProgramAttributeBrushIndex]);
    glVertexAttribPointer(_drawingProgramAttributes[kDrawingProgramAttributeBrushIndex],
                          2,
                          GL_FLOAT,
                          0,
                          0,
                          pointsSet.brushIndexes);
    
    glEnableVertexAttribArray(_drawingProgramAttributes[kDrawingProgramAttributeBrushWidth]);
    glVertexAttribPointer(_drawingProgramAttributes[kDrawingProgramAttributeBrushWidth],
                          1,
                          GL_FLOAT,
                          0,
                          0,
                          pointsSet.widths);
    
    //updating uniforms
    glUniformMatrix4fv(_drawingProgramUniforms[kDrawingProgramUniformProjectionMatrix],
                       1,
                       GL_FALSE,
                       _combinedProjectionModelViewMatrix.m);
    

    glUniform4fv(_drawingProgramUniforms[kDrawingProgramUniformPointColor],
                1,
                _brushColor);
    
    glUniform2fv(_drawingProgramUniforms[kDrawingProgramUniformSingeBrushSizeInAtlas],
                 1,
                 _brushes[_currentBrushType].textureAtlasParams);
    
    
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D,  _brushes[_currentBrushType].texture.name);
    glUniform1i(_drawingProgramUniforms[kDrawingProgramUniformTextureName], 2);
    
    //drawing
    glDrawArrays(GL_POINTS, 0, (GLsizei)pointsSet.size);
    
    glDisableVertexAttribArray(_drawingProgramAttributes[kDrawingProgramAttributeVertexes]);
    glDisableVertexAttribArray(_drawingProgramAttributes[kDrawingProgramAttributeBrushIndex]);
    glDisableVertexAttribArray(_drawingProgramAttributes[kDrawingProgramAttributeBrushWidth]);
    
    
}

- (void)drawTexture:(GLuint)texture
{
    static GLfloat vertixes[8] = {0};
    static GLfloat texCoords[8] =
    {
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
    };

    vertixes[0] = 0;
    vertixes[1] = 0;
    vertixes[2] = _pixelsSize.width;
    vertixes[3] = 0;
    vertixes[4] = 0;
    vertixes[5] = _pixelsSize.height;
    vertixes[6] = _pixelsSize.width;
    vertixes[7] = _pixelsSize.height;
    
    [GPUImageOpenGLESContext setActiveShaderProgram: _texturePresentingProgram];
    
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(_texturePresentingProgramUniforms[kTexturePresentingProgramUniformTextureName], 4);
    
    glUniformMatrix4fv(_texturePresentingProgramUniforms[kTexturePresentingProgramUniformProjectionMatrix],
                       1,
                       GL_FALSE,
                       _combinedProjectionModelViewMatrix.m);
    
    glEnableVertexAttribArray(_texturePresentingProgramAttributes[kTexturePresentingProgramAttributeVertexes]);
    glVertexAttribPointer(_texturePresentingProgramAttributes[kTexturePresentingProgramAttributeVertexes],
                          2,
                          GL_FLOAT,
                          0,
                          0,
                          vertixes);
    
    glEnableVertexAttribArray(_texturePresentingProgramAttributes[kTexturePresentingProgramAttributeTextureCoords]);
    glVertexAttribPointer(_texturePresentingProgramAttributes[kTexturePresentingProgramAttributeTextureCoords],
                          2,
                          GL_FLOAT,
                          0,
                          0,
                          texCoords);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glDisableVertexAttribArray(_texturePresentingProgramAttributes[kTexturePresentingProgramAttributeVertexes]);
    glDisableVertexAttribArray(_texturePresentingProgramAttributes[kTexturePresentingProgramAttributeTextureCoords]);
}

- (void)interpolatePointsFrom:(CGPoint)start
                           to:(CGPoint)end
                        force:(GLfloat)force
                     maxForce:(GLfloat)maxForce
                  inPointsSet:(FancyDrawingViewPointsSet*)pointsSet
       bypassWidthCalculation:(BOOL)bypassWidthCalculation
{
//    NSLog(@"__________________________interpolation____________________________");
//    NSLog(@"start %f, %f", start.x, start.y);
//    NSLog(@"end %f, %f", end.x, end.y);
    //converting point to scaled ccords
    start.x *= _scale;
    start.y *= _scale;
    end.x *= _scale;
    end.y *= _scale;
    
    CGPoint delta;
    delta.x = end.x - start.x;
    delta.y = end.y - start.y;
    
    //select brush index
    CGPoint brushIndex = CGPointMake(0, 0);

    float lineLength = sqrtf(delta.x * delta.x + delta.y * delta.y);
    
    if(lineLength < 2)
    {
        lineLength = 2;
    }
    
    //calculate line width based on the hetch speed
    
    float resultLineWidth = _lastInterpolatedLineWidth / 2.0f;
    
    if(!bypassWidthCalculation)
    {
        float lineWidth = MAX_LINE_WIDTH / 1.4 / lineLength;
        
        lineWidth = MAX(MIN_LINE_WIDTH, MIN(lineWidth, MAX_LINE_WIDTH));
        
        if(force > 0.05)
        {
             lineWidth *= powf(force * 3.5, 1.0);
        }
        
        //with force we allow to make line thiker and thiner
        lineWidth = MAX(MIN_LINE_WIDTH, MIN(lineWidth, MAX_LINE_WIDTH * 2));
        
        //low pass filtering
        _lastInterpolatedLineWidth = 0.98 * _lastInterpolatedLineWidth + 0.02 * lineWidth;
        resultLineWidth = _lastInterpolatedLineWidth;
    }

    
//    NSLog(@"resultLineWidth %f", resultLineWidth);
    //interpolating between start and end points
    NSInteger pointsCount = MAX(ceilf(lineLength) / BRUSH_PIXEL_STEP, 1);
//    NSLog(@"pointsCount %ld", (long)pointsCount);
//    NSLog(@"___________________________________________________________________");
    //added for drawing offset prediction
    int startIterator = 1;
    if (_touchesForPrediction.count != NUM_POINTS_TO_APPROXIMATE)
    {
        startIterator = 0;
    }
    for(int i = startIterator; i < pointsCount; ++i)
    {
        if(pointsSet.size >= pointsSet.capacity)
        {
            break;
        }
        
        pointsSet.points[2 * pointsSet.size + 0] = round(start.x + (end.x - start.x) * ((GLfloat)i / (GLfloat)pointsCount));
        pointsSet.points[2 * pointsSet.size + 1] = round(start.y + (end.y - start.y) * ((GLfloat)i / (GLfloat)pointsCount));
//        NSLog(@"%d. %f, %f", i, pointsSet.points[2 * pointsSet.size + 0], pointsSet.points[2 * pointsSet.size + 1]);
        pointsSet.brushIndexes[2 * pointsSet.size + 0] = brushIndex.x;
        pointsSet.brushIndexes[2 * pointsSet.size + 1] = brushIndex.y;
        
        pointsSet.widths[pointsSet.size] = round(resultLineWidth);
        
        pointsSet.size += 1;
    }
}

- (void)movePredictedTouchcesToRealTouches
{
    for (int i = 0; i < _predictedPoints.size; i++)
    {
        if(_bufferedPoints.size >= _bufferedPoints.capacity)
        {
            break;
        }
        
        _bufferedPoints.points[2 * _bufferedPoints.size] = _predictedPoints.points[2 * i];
        _bufferedPoints.points[2 * _bufferedPoints.size + 1] = _predictedPoints.points[2 * i + 1];
        _bufferedPoints.size += 1;
    }
    
    _predictedPoints.size = 0;
}

#pragma mark Setup Calls

- (BOOL)loadDrawingProgram
{
    _drawingProgram = [self loadProgramWithName: @"point"
                              andTrackAttrbutes: @[@"inVertex", @"inBrushIndexesToUse", @"inBrushWidth"]];

    if(!_drawingProgram)
    {
        return NO;
    }
    
    _drawingProgramAttributes[kDrawingProgramAttributeVertexes] = [_drawingProgram attributeIndex:@"inVertex"];
    _drawingProgramAttributes[kDrawingProgramAttributeBrushIndex] = [_drawingProgram attributeIndex:@"inBrushIndexesToUse"];
    _drawingProgramAttributes[kDrawingProgramAttributeBrushWidth] = [_drawingProgram attributeIndex:@"inBrushWidth"];
    
    _drawingProgramUniforms[kDrawingProgramUniformProjectionMatrix] = [_drawingProgram uniformIndex: @"modelViewMatrix"];
    _drawingProgramUniforms[kDrawingProgramUniformPointColor] = [_drawingProgram uniformIndex: @"pointColor"];
    _drawingProgramUniforms[kDrawingProgramUniformTextureName] = [_drawingProgram uniformIndex: @"texture"];
    _drawingProgramUniforms[kDrawingProgramUniformSingeBrushSizeInAtlas] = [_drawingProgram uniformIndex: @"brushesAtlasSingeItemSize"];
    
    return YES;
}

- (BOOL)loadPresentingProgram
{
    _texturePresentingProgram = [self loadProgramWithName: @"default"
                              andTrackAttrbutes: @[@"inVertex", @"inTextureCoord"]];
    
    if(!_drawingProgram)
    {
        return NO;
    }
    
    _texturePresentingProgramAttributes[kTexturePresentingProgramAttributeVertexes] = [_texturePresentingProgram attributeIndex:@"inVertex"];
    _texturePresentingProgramAttributes[kTexturePresentingProgramAttributeTextureCoords] = [_texturePresentingProgram attributeIndex:@"inTextureCoord"];
    
    _texturePresentingProgramUniforms[kTexturePresentingProgramUniformTextureName] = [_texturePresentingProgram uniformIndex: @"texture"];
    
    _texturePresentingProgramUniforms[kTexturePresentingProgramUniformProjectionMatrix] = [_texturePresentingProgram uniformIndex: @"modelViewMatrix"];
    
    return YES;
}

- (GLProgram*)loadProgramWithName:(NSString*)name
                andTrackAttrbutes:(NSArray*)attributes
{

    NSString* shaderRootDirectory = [[NSBundle mainBundle].bundlePath stringByAppendingPathComponent:@"Shaders"];
    
    
    NSString* backgroundVertexShader = [NSString stringWithContentsOfFile: [shaderRootDirectory stringByAppendingFormat: @"/%@.vsh", name]
                                                                 encoding: NSUTF8StringEncoding
                                                                    error: nil];
    
    NSString* backgroundFragmentShader = [NSString stringWithContentsOfFile: [shaderRootDirectory stringByAppendingFormat: @"/%@.fsh", name]
                                                                   encoding: NSUTF8StringEncoding
                                                                      error: nil];
    
    GLProgram* program = [[GPUImageOpenGLESContext sharedImageProcessingOpenGLESContext]
                                programForVertexShaderString: backgroundVertexShader
                                        fragmentShaderString: backgroundFragmentShader];
    
    
    if (!program.initialized)
    {
        
        for(NSString* attribure in attributes)
        {
            [program addAttribute: attribure];
        }
        
        if (![program link])
        {
            NSString *progLog = [program programLog];
            NSLog(@"Program link log: %@", progLog);
            NSString *fragLog = [program fragmentShaderLog];
            NSLog(@"Fragment shader compile log: %@", fragLog);
            NSString *vertLog = [program vertexShaderLog];
            NSLog(@"Vertex shader compile log: %@", vertLog);
            program = nil;
            NSAssert(NO, @"Filter shader link failed");
            return nil;
        }
    }
    
    return program;
}

- (FancyDrawingViewTextureInfo)loadTextureFromUIImage:(UIImage*)image
{
    CGImageRef		CGImage;
    CGContextRef	context;
    GLubyte			*rawData;
    size_t			width, height;
    GLuint          texId;
    FancyDrawingViewTextureInfo   texture;
    
    // First create a UIImage object from the data in a image file, and then extract the Core Graphics image
    CGImage = image.CGImage;
    
    // Get the width and height of the image
    width = CGImageGetWidth(CGImage);
    height = CGImageGetHeight(CGImage);
    
    // Make sure the image exists
    if(CGImage)
    {
        // Allocate  memory needed for the bitmap context
        rawData = (GLubyte *) calloc(width * height * 4, sizeof(GLubyte));
        // Use  the bitmatp creation function provided by the Core Graphics framework.
        context = CGBitmapContextCreate(rawData, width, height, 8, width * 4, CGImageGetColorSpace(CGImage), (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
        // After you create the context, you can draw the  image to the context.
        CGContextDrawImage(context, CGRectMake(0.0, 0.0, (CGFloat)width, (CGFloat)height), CGImage);
        // You don't need the context at this point, so you need to release it to avoid memory leaks.
        CGContextRelease(context);
        // Use OpenGL ES to generate a name for the texture.
        glGenTextures(1, &texId);
        // Bind the texture name.
        glBindTexture(GL_TEXTURE_2D, texId);
        // Set the texture parameters to use a minifying filter and a linear filer (weighted average)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        // Specify a 2D texture image, providing the a pointer to the image data in memory
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int)width, (int)height, 0, GL_RGBA, GL_UNSIGNED_BYTE, rawData);
        // Release  the image data; it's no longer needed
        free(rawData);
        
        texture.name = texId;
        texture.width = (int)width;
        texture.height = (int)height;
    }
    
    return texture;
}

#pragma mark Managing Display FBO
//all these methods should be called in the video processing thread!

- (void)createDisplayFramebuffer
{
    [GPUImageOpenGLESContext useImageProcessingContext];
    
    glGenFramebuffers(1, &_displayFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _displayFramebuffer);
    
    glGenRenderbuffers(1, &_displayRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _displayRenderbuffer);
    
    [[[GPUImageOpenGLESContext sharedImageProcessingOpenGLESContext] context] renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    
    GLint backingWidth, backingHeight;
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
    
    if ( (backingWidth == 0) || (backingHeight == 0) )
    {
        [self destroyDisplayFramebuffer];
        return;
    }
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _displayRenderbuffer);
    
    GLuint framebufferCreationStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    NSAssert(framebufferCreationStatus == GL_FRAMEBUFFER_COMPLETE, @"Failure with display framebuffer generation for display of size: %f, %f",
             self.bounds.size.width,
             self.bounds.size.height);
}

- (void)destroyDisplayFramebuffer
{
    [GPUImageOpenGLESContext useImageProcessingContext];
    
    if (_displayFramebuffer)
    {
        glDeleteFramebuffers(1, &_displayFramebuffer);
        _displayFramebuffer = 0;
    }
    
    if (_displayRenderbuffer)
    {
        glDeleteRenderbuffers(1, &_displayRenderbuffer);
        _displayRenderbuffer = 0;
    }
}

- (void)setDisplayFramebuffer
{
    if (!_displayFramebuffer)
    {
        [self createDisplayFramebuffer];
    }
    
    glBindFramebuffer(GL_FRAMEBUFFER, _displayFramebuffer);
    glViewport(0, 0, (GLint)self.bounds.size.width, (GLint)self.bounds.size.height);
    
    [self clearGL];
}

- (void)presentFramebuffer
{
    glBindRenderbuffer(GL_RENDERBUFFER, _displayRenderbuffer);
    [[GPUImageOpenGLESContext sharedImageProcessingOpenGLESContext] presentBufferForDisplay];
}


@end

@implementation FancyDrawingViewPointsSet

- (instancetype)initWithCapacity:(NSUInteger)capacity
{
    if(self = [super init])
    {
        _capacity = capacity;
        
        _points = malloc(sizeof(GLfloat) * 2 * capacity);
        _brushIndexes = malloc(sizeof(GLfloat) * 2 * capacity);
        _widths = malloc(sizeof(GLfloat) * capacity);
        _opacities = malloc(sizeof(GLfloat) * capacity);
        
        self.size = 0;
        
    }
    
    return self;
}

- (void)dealloc
{
    if(_points)
    {
        free(_points);
    }
    
    if(_brushIndexes)
    {
        free(_brushIndexes);
    }
    
    if(_widths)
    {
        free(_widths);
    }
    
    if(_opacities)
    {
        free(_opacities);
    }
}

@end

