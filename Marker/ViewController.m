//
//  ViewController.m
//  Marker
//
//  Created by Vitaliy Kolomoets on 3/24/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import "ViewController.h"
#import "MarkerTouch.h"
#import "CalibrationLogic.h"
#import "CalibrationResult.h"
@import CoreBluetooth;
@import AVFoundation;

#define DISPLAY_DRAWING

static int numOfActiveStatesToTrigger = 1;

typedef enum
{
    MarkerColor1 = 2,
    MarkerColor2 = 4,
    MarkerColor3 = 6,
} MarkerColor;


@interface ViewController ()<CBCentralManagerDelegate, CBPeripheralDelegate, UITableViewDelegate, UITableViewDataSource, CalibrationLogicDelegate>
{
    CBCentralManager*               _centralManager;
    NSString*                       _logString;
    CADisplayLink*                  _displayLink;
    CGPoint                         _bottomLeftCornerPoint;
    CGPoint                         _upperRightCornerPoint;
    CGPoint                         _markerPosition;
    MarkerState                     _state;
    MarkerColor                     _color;
    NSMutableArray<CBPeripheral*>*  _devices;
    NSUInteger                      _currentNumOfActiveStates;
    CalibrationLogic*               _calibrationLogic;
    BOOL                            _waitingForMarkerUp;
//    AVAudioPlayer*                  _audioPlayer;
}
@property (strong) CBPeripheral*    peripheral;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _centralManager = [[CBCentralManager alloc] initWithDelegate:self
                                                          queue:nil
                                                        options:nil];
    _devices = [NSMutableArray new];
    
    [self.devicesListTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellID"];
    self.devicesListTableView.delegate = self;
    self.devicesListTableView.dataSource = self;
    _logString = [NSMutableString new];
    _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(onDisplayLink)];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    
    [self.disconnectButton addTarget:self
                              action:@selector(restartScanning)
                    forControlEvents:UIControlEventPrimaryActionTriggered];
    
    self.disconnectButton.hidden = YES;
    
    [self.recalibrateButton addTarget:self
                              action:@selector(recalibrate)
                    forControlEvents:UIControlEventPrimaryActionTriggered];
    
    self.recalibrateButton.hidden = YES;
    
    self.markerView.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidEnterBackgroundNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      if(self.peripheral)
                                                      {
                                                          [_centralManager cancelPeripheralConnection:self.peripheral];
                                                          self.peripheral = nil;
                                                      }
                                                      [_devices removeAllObjects];
                                                      _devicesListTableView.hidden = NO;
                                                      _disconnectButton.hidden = YES;
                                                      _recalibrateButton.hidden = YES;
                                                      [_markerView cleanUp];
                                                      _markerView.hidden = YES;
                                                      [_devicesListTableView reloadData];
                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillEnterForegroundNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      _logString = @"";
                                                        [_centralManager scanForPeripheralsWithServices:nil options:nil];
                                                  }];
    
    _calibrationLogic = [CalibrationLogic new];
    _calibrationLogic.delegate = self;
}

-(void)viewDidLayoutSubviews
{
    [_markerView drawGrid];
    if(_calibrationLogic.state == CalibrationLogicStateInited)
    {
        [_calibrationLogic startCalibration];
    }
}

- (void)onDisplayLink
{
    if(_markerView.hidden == YES)
    {
        _logTextView.text = _logString;
    }
}

- (void)restartScanning
{
    if(self.peripheral)
    {
        [_centralManager cancelPeripheralConnection:self.peripheral];
        self.peripheral = nil;
    }
    [_devices removeAllObjects];
    _devicesListTableView.hidden = NO;
    _disconnectButton.hidden = YES;
    _recalibrateButton.hidden = YES;
    self.markerView.hidden = YES;
    [self.markerView cleanUp];
    [_devicesListTableView reloadData];
    [_centralManager scanForPeripheralsWithServices:nil options:nil];
    _logString = @"";
}

- (void)recalibrate
{
    [CalibrationResult clear];
    _calibrationLogic = [CalibrationLogic new];
    _calibrationLogic.delegate = self;
    [_calibrationLogic startCalibration];
}

#pragma mark -

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    [self addTextRow:@"Central Manager did update state to: "];
    NSLog(@"Central Manager did update state to: ");
    
    switch (central.state)
    {
        case CBCentralManagerStateUnknown:
        {
            NSLog(@"CBCentralManagerStateUnknown - State unknown, update imminent");
            [self addTextRow:@"CBCentralManagerStateUnknown - State unknown, update imminent"];
        }
            break;
            
        case CBCentralManagerStateResetting:
        {
            NSLog(@"CBCentralManagerStateResetting - The connection with the system service was momentarily lost, update imminent");
            [self addTextRow:@"CBCentralManagerStateResetting - The connection with the system service was momentarily lost, update imminent"];
        }
            break;
            
        case CBCentralManagerStateUnsupported:
        {
            NSLog(@"CBCentralManagerStateUnsupported - The platform doesn't support the Bluetooth Low Energy Central/Client role");
            [self addTextRow:@"CBCentralManagerStateUnsupported - The platform doesn't support the Bluetooth Low Energy Central/Client role"];
        }
            break;
            
        case CBCentralManagerStateUnauthorized:
        {
            NSLog(@"CBCentralManagerStateUnauthorized - The application is not authorized to use the Bluetooth Low Energy Central/Client role.");
            [self addTextRow:@"CBCentralManagerStateUnauthorized - The application is not authorized to use the Bluetooth Low Energy Central/Client role."];
        }
            break;
            
        case CBCentralManagerStatePoweredOff:
        {
            NSLog(@"CBCentralManagerStatePoweredOff - Bluetooth is currently powered off");
            [self addTextRow:@"CBCentralManagerStatePoweredOff - Bluetooth is currently powered off"];
        }
            break;
            
        case CBCentralManagerStatePoweredOn:
        {
            NSLog(@"CBCentralManagerStatePoweredOn - Bluetooth is currently powered on and available to use");
            [self addTextRow:@"CBCentralManagerStatePoweredOn - Bluetooth is currently powered on and available to use"];
            [self addTextRow:@"Start scanning..."];
            [_centralManager scanForPeripheralsWithServices:nil options:nil];
        }
            break;
            
        default:
            break;
    }
    
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *, id> *)advertisementData RSSI:(NSNumber *)RSSI
{
//    if(![peripheral.name isEqualToString:@"SmartphoneGun"])
//    {
//        return;
//    }
    BOOL alreadyDiscovered = NO;
    for (CBPeripheral* device in _devices)
    {
        if([device.identifier.UUIDString isEqualToString:peripheral.identifier.UUIDString])
        {
            alreadyDiscovered = YES;
            break;
        }
    }
    
    if(!alreadyDiscovered)
    {
        [_devices addObject:peripheral];
        [self addTextRow:[NSString stringWithFormat:@"Сentral Manager did discover Peripheral with name: %@", peripheral.name]];
        [self.devicesListTableView reloadData];
    }
}


- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral;
{
    [self addTextRow:[NSString stringWithFormat:@"Сentral Manager did connect Peripheral with name: %@", peripheral.name]];
    peripheral.delegate = self;
    [self addTextRow:@"Trying to discover services..."];
    [peripheral discoverServices:nil];
    self.disconnectButton.hidden = NO;
    self.recalibrateButton.hidden = NO;
#ifdef DISPLAY_DRAWING
    self.markerView.hidden = NO;
#endif
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(nullable NSError *)error
{
    [self addTextRow:@"Сentral Manager did fail to connect peripheral"];
    if(error)
    {
        [self addTextRow:[NSString stringWithFormat:@"with error: %@", [error description]]];
    }
    
    [self addTextRow:@"Restart scanning..."];
    [_centralManager scanForPeripheralsWithServices:nil options:nil];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(nullable NSError *)error
{
    [self addTextRow:@"Сentral Manager did disconnect peripheral"];
    if(error)
    {
        [self addTextRow:[NSString stringWithFormat:@"with error: %@", [error description]]];
    }
    [self addTextRow:@"Restart scanning..."];
    [_centralManager scanForPeripheralsWithServices:nil options:nil];
}

#pragma mark -

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(nullable NSError *)error
{
    if(!error)
    {
        [self addTextRow:@"Сentral Manager did discover services"];
        
        for (CBService *service in peripheral.services)
        {
            [self addTextRow:[NSString stringWithFormat:@"%lu. %@", [peripheral.services indexOfObject:service], [service description]]];
            [self.peripheral discoverCharacteristics:nil forService:service];
        }
    }
    else
    {
        [self addTextRow:@"Failed to discover services"];
        [self addTextRow:[error description]];
    }
}


#warning check the characteristic name
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    for (CBCharacteristic *characteristic in service.characteristics)
    {
        [self addTextRow:[NSString stringWithFormat:@"Discovered characteristic %@ for service %@", characteristic, service]];
        if(characteristic.properties & CBCharacteristicPropertyNotify && [characteristic.UUID.UUIDString isEqualToString:@"FF01"])// || [characteristic.UUID.UUIDString isEqualToString:@"FF02"]))//
        {
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
        }
        else
        {
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral
didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic
             error:(NSError *)error {
    
    if (error) {
        NSLog(@"Error changing notification state: %@ forCharacteristic %@",
              [error localizedDescription], characteristic.UUID.UUIDString);
    }
    else
    {
        NSLog(@"subscribed to characteristic: %@", characteristic.UUID.UUIDString);
    }
    
}

#warning raw data
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if(!error)
    {
        UInt8* bytePtr = (UInt8*)[characteristic.value bytes];
        SInt16* wordPtr = (SInt16*)((UInt8*)[characteristic.value bytes] + 1);

        NSMutableString* dataStr = [NSMutableString new];
        
        int state = (*bytePtr) & 0x40;
        state = state >> 6;
        int color = (*bytePtr) & 0x03;
//        color = color >> 1;
        _markerPosition.x = (SInt16)ntohs(*wordPtr);
        _markerPosition.y = (SInt16)ntohs(*(wordPtr + 1));
//        NSLog(@"x = %.2f, y = %.2f", _markerPosition.x, _markerPosition.y);
//        [dataStr appendFormat:@"%d, %d, %f, %f", state, color, _markerPosition.x, _markerPosition.y];
//        NSLog(@"%@", dataStr);
//        NSLog(@"%@", characteristic.value.description);
        [self addTextRow:dataStr];
        if(state && _state == MarkerStateIdle)
        {
            _currentNumOfActiveStates++;
            if(_currentNumOfActiveStates >= numOfActiveStatesToTrigger)
            {
                _state = MarkerStateActive;
                _currentNumOfActiveStates = 0;
            }
        }
        else if(!state && _state == MarkerStateActive)
        {
            _currentNumOfActiveStates++;
            if(_currentNumOfActiveStates >= numOfActiveStatesToTrigger)
            {
                _state = MarkerStateIdle;
                _currentNumOfActiveStates = 0;
                _waitingForMarkerUp = NO;
            }
        }
        
        MarkerTouch* touch = [[MarkerTouch alloc] init];
        touch.state = state;
        
        if(_calibrationLogic.state == CalibrationLogicStateCalibrated)
        {
            touch.currentPosition = [_calibrationLogic.calibrationResult screenPointForMarkerPoint:_markerPosition];
            switch (color)
            {
                case 1:
                    touch.color = [UIColor greenColor];
                    break;
                    
                case 2:
                    touch.color = [UIColor blueColor];
                    break;
                    
                case 3:
                    touch.color = [UIColor whiteColor];//color of the background
                    break;
                    
                default:
                    touch.color = [UIColor redColor];
                    break;
            }
            _markerView.touch = touch;
            _markerView.markerIsActive = _state;
        }
        else if(_state && !_waitingForMarkerUp)
        {
            touch.currentPosition = _markerPosition;
            [_calibrationLogic onMarkerTouchReceived:touch];
        }
    }
}

#pragma mark -

- (void)onCalibrationLogic:(CalibrationLogic*)cLogic stateDidChangeFromState:(CalibrationLogicState)oldState
{
    CalibrationLogicState newState = cLogic.state;
    
    switch (newState)
    {
        case CalibrationLogicStateInited:
        case CalibrationLogicStateCalibrated:
        {
            
        }
            break;
            
        default:
        {
            [_markerView setCalibrationPoint:cLogic.currentCalibrationPoint];
            if(_state)
            {
                _waitingForMarkerUp = YES;
            }
        }
            break;
    }
}

#pragma mark -

- (void)addTextRow:(NSString*)str
{
    NSArray* rows = [_logTextView.text componentsSeparatedByString:@"\n"];
    if(rows.count >= 35)
    {
        NSRange eof = [_logTextView.text rangeOfString:@"\n"];
        NSString* newText = [_logTextView.text substringFromIndex:eof.location + eof.length];
        _logString = [newText stringByAppendingFormat:@"%@\n", str];
    }
    else
    {
        _logString = [_logString stringByAppendingFormat:@"%@\n", str];
    }
}

#pragma mark -

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_centralManager stopScan];
    [self addTextRow:@"Scanning stopped, trying to connect..."];
    self.peripheral = [_devices objectAtIndex:indexPath.row];
    [_centralManager connectPeripheral:self.peripheral options:nil];
    [_devices removeAllObjects];
    self.devicesListTableView.hidden = YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _devices.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
    }
    if(_devices[indexPath.row].name)
    {
        cell.textLabel.text = _devices[indexPath.row].name;
    }
    else
    {
        cell.textLabel.text = _devices[indexPath.row].identifier.UUIDString;
    }
    return cell;
}
@end
