
attribute vec4   inVertex;
attribute vec2   inBrushIndexesToUse;
attribute float  inBrushWidth;

uniform mat4 modelViewMatrix;
uniform vec4 pointColor;

varying lowp vec4  color;
varying lowp vec2  brushIndexesToUse;

void main()
{
	gl_Position = modelViewMatrix * inVertex;
    gl_PointSize = inBrushWidth;

    color = pointColor;
    brushIndexesToUse = inBrushIndexesToUse;
}
