//
//  MarkerView.h
//  Marker
//
//  Created by Vitaliy Kolomoets on 3/28/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalibrationPoint.h"

@class MarkerTouch;

typedef enum
{
    MarkerViewStateInited,
    MarkerViewStateCalibratingBL,
    MarkerViewStateCalibratingUR,
    MarkerViewStateDrawing
}MarkerViewState;

@interface MarkerView : UIView

@property UIImageView*                          reticule;
@property (nonatomic) BOOL                      markerIsActive;
@property (nonatomic) MarkerTouch*              touch;

- (void)cleanUp;
- (void)drawGrid;
- (void)setCalibrationPoint:(CalibrationPoint*)point;
@end
