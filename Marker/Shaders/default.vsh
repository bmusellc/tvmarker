
attribute vec4  inVertex;
attribute vec4  inTextureCoord;

uniform mat4 modelViewMatrix;

varying vec2 textureCoordinate;

void main()
{
    gl_Position = modelViewMatrix * inVertex;
    textureCoordinate = inTextureCoord.xy;
}
