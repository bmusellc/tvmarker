//
//  CalibrationResult.m
//  Marker
//
//  Created by Vitaliy Kolomoets on 8/16/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import "CalibrationResult.h"
#import "CalibrationRectangle.h"
#import "VectorMath.h"

@implementation CalibrationResult
{
    NSMutableArray<CalibrationRectangle*>*     _rectangles;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _points = [NSMutableArray new];
        _rectangles = [NSMutableArray new];
    }
    return self;
}

+ (instancetype)savedResult
{
    CalibrationResult* result = [CalibrationResult new];
    NSMutableArray* savedData = [NSMutableArray arrayWithContentsOfURL:[self _calibrationFileURL]];
    for (NSDictionary* dict in savedData)
    {
        CalibrationPoint* p = [CalibrationPoint new];
        p.screenCoords = CGPointFromString(dict[@"screen"]);
        p.calibrationCoords = CGPointFromString(dict[@"calibrated"]);
        p.calibrated = YES;
        [result->_points addObject:p];
    }
    if(result->_points.count)
    {
        [result _processLoadedData];
    }
    else
    {
        result = nil;
    }
    return result;
}

+ (void)clear
{
    [[NSFileManager defaultManager] removeItemAtURL:[self _calibrationFileURL] error:nil];
}

- (CGPoint)screenPointForMarkerPoint:(CGPoint)markerPoint
{
    __block BOOL pointInsideCalibrationRect = NO;
    __block CGPoint result = CGPointZero;

    [_rectangles enumerateObjectsUsingBlock:^(CalibrationRectangle * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
    {
        if(CGRectContainsPoint(obj.rectangle, markerPoint))
        {   
            result = [obj calculateScreenCoordinatedForMarkerPoint:markerPoint];
            
            pointInsideCalibrationRect = YES;
            *stop = YES;
        }
    }];

    
    if(!pointInsideCalibrationRect)
    {
        if(markerPoint.x < _points[8].calibrationCoords.x)
        {
            if(markerPoint.y < _points[8].calibrationCoords.y)
            {
                result = [_rectangles[0] calculateScreenCoordinatedForMarkerPoint:markerPoint];
            }
            else
            {
                result = [_rectangles[3] calculateScreenCoordinatedForMarkerPoint:markerPoint];
            }
        }
        else
        {
            if(markerPoint.y < _points[8].calibrationCoords.y)
            {
                result = [_rectangles[1] calculateScreenCoordinatedForMarkerPoint:markerPoint];
            }
            else
            {
                result = [_rectangles[2] calculateScreenCoordinatedForMarkerPoint:markerPoint];
            }
        }
    }
    
    return result;
}

-(void)processData
{
    [_rectangles removeAllObjects];

#ifdef NINE_POINTS_CALIBRATION
    CGFloat centerX = _points[0].calibrationCoords.x + (_points[2].calibrationCoords.x - _points[0].calibrationCoords.x) * 0.5 + _points[7].calibrationCoords.x + (_points[3].calibrationCoords.x - _points[7].calibrationCoords.x) * 0.5 + _points[6].calibrationCoords.x + (_points[4].calibrationCoords.x - _points[6].calibrationCoords.x) * 0.5 + _points[1].calibrationCoords.x + _points[8].calibrationCoords.x + _points[5].calibrationCoords.x;
    centerX /= 6.0;
    
    CGFloat centerY = _points[2].calibrationCoords.y + (_points[4].calibrationCoords.y - _points[2].calibrationCoords.y) * 0.5 + _points[1].calibrationCoords.y + (_points[5].calibrationCoords.y - _points[1].calibrationCoords.y) * 0.5 + _points[0].calibrationCoords.y + (_points[6].calibrationCoords.y - _points[0].calibrationCoords.y) * 0.5 + _points[7].calibrationCoords.y + _points[8].calibrationCoords.y + _points[3].calibrationCoords.y;
    centerY /= 6.0;
    
    CGFloat width = (_points[2].calibrationCoords.x - _points[0].calibrationCoords.x) + (_points[3].calibrationCoords.x - _points[7].calibrationCoords.x) + (_points[4].calibrationCoords.x - _points[6].calibrationCoords.x);
    width /= 3.0;
    
    CGFloat height = (_points[4].calibrationCoords.y - _points[2].calibrationCoords.y) + (_points[5].calibrationCoords.y - _points[1].calibrationCoords.y) + (_points[6].calibrationCoords.y - _points[0].calibrationCoords.y);
    height /= 3.0;
#else
    CGFloat xOriginCoord = (_points[0].calibrationCoords.x + _points[3].calibrationCoords.x) * 0.5;
    
    CGFloat width = (_points[1].calibrationCoords.x + _points[2].calibrationCoords.x) * 0.5 - xOriginCoord;
    
    CGFloat centerX = xOriginCoord + width * 0.5;
    
    CGFloat yOriginCoord = (_points[0].calibrationCoords.y + _points[1].calibrationCoords.y) * 0.5;
    
    CGFloat height = (_points[2].calibrationCoords.y + _points[3].calibrationCoords.y) * 0.5 - yOriginCoord;
    
    CGFloat centerY = yOriginCoord + height * 0.5;
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    UIEdgeInsets calibrationInsets = UIEdgeInsetsMake(_points[0].screenCoords.y / screenSize.height,
                                                      _points[0].screenCoords.x / screenSize.width,
                                                      (screenSize.height - _points[2].screenCoords.y) / screenSize.height,
                                                      (screenSize.width - _points[2].screenCoords.x) / screenSize.width);
    
    CGSize calibrationSize = CGSizeMake(screenSize.width * (1 - calibrationInsets.left - calibrationInsets.right), screenSize.height * (1 - calibrationInsets.top - calibrationInsets.bottom));
    
    CalibrationPoint* ucPoint = [CalibrationPoint new];//UC
    ucPoint.screenCoords = CGPointMake(screenSize.width * calibrationInsets.left + calibrationSize.width * 0.5, calibrationInsets.top * screenSize.height);
    [_points insertObject:ucPoint atIndex:1];
    
    CalibrationPoint* crPoint = [CalibrationPoint new];//CR
    crPoint.screenCoords = CGPointMake(screenSize.width * (1.0 - calibrationInsets.right), screenSize.height * calibrationInsets.top + calibrationSize.height * 0.5);
    [_points insertObject:crPoint atIndex:3];
    
    CalibrationPoint* bcPoint = [CalibrationPoint new];//BC
    bcPoint.screenCoords = CGPointMake(screenSize.width * calibrationInsets.left + calibrationSize.width * 0.5, screenSize.height * (1.0 - calibrationInsets.bottom));
    [_points insertObject:bcPoint atIndex:5];
    
    CalibrationPoint* clPoint = [CalibrationPoint new];//CL
    clPoint.screenCoords = CGPointMake(calibrationInsets.left * screenSize.width, screenSize.height * calibrationInsets.top + calibrationSize.height * 0.5);
    [_points addObject:clPoint];
    
    CalibrationPoint* cPoint = [CalibrationPoint new];//C
    cPoint.screenCoords = CGPointMake(screenSize.width * calibrationInsets.left + calibrationSize.width * 0.5, screenSize.height * calibrationInsets.top +  calibrationSize.height * 0.5);
    [_points addObject:cPoint];
    
#endif
    
    _points[0].calibrationCoords = CGPointMake(centerX - width * 0.5, centerY - height * 0.5);
    _points[1].calibrationCoords = CGPointMake(centerX, centerY - height * 0.5);
    _points[2].calibrationCoords = CGPointMake(centerX + width * 0.5, centerY - height * 0.5);
    _points[7].calibrationCoords = CGPointMake(centerX - width * 0.5, centerY);
    _points[8].calibrationCoords = CGPointMake(centerX, centerY);
    _points[3].calibrationCoords = CGPointMake(centerX + width * 0.5, centerY);
    _points[6].calibrationCoords = CGPointMake(centerX - width * 0.5, centerY + height * 0.5);
    _points[5].calibrationCoords = CGPointMake(centerX, centerY + height * 0.5);
    _points[4].calibrationCoords = CGPointMake(centerX + width * 0.5, centerY + height * 0.5);
    
    NSMutableArray* calibrationPoints = [NSMutableArray new];
    [calibrationPoints addObject:_points[0]];
    [calibrationPoints addObject:_points[1]];
    [calibrationPoints addObject:_points[8]];
    [calibrationPoints addObject:_points[7]];
    CalibrationRectangle* rect = [[CalibrationRectangle alloc] initWithCalibrationPoints:calibrationPoints anchorPointIndex:2];
    [_rectangles addObject:rect];
    
    calibrationPoints = [NSMutableArray new];
    [calibrationPoints addObject:_points[1]];
    [calibrationPoints addObject:_points[2]];
    [calibrationPoints addObject:_points[3]];
    [calibrationPoints addObject:_points[8]];
    rect = [[CalibrationRectangle alloc] initWithCalibrationPoints:calibrationPoints anchorPointIndex:3];
    [_rectangles addObject:rect];
    
    calibrationPoints = [NSMutableArray new];
    [calibrationPoints addObject:_points[8]];
    [calibrationPoints addObject:_points[3]];
    [calibrationPoints addObject:_points[4]];
    [calibrationPoints addObject:_points[5]];
    rect = [[CalibrationRectangle alloc] initWithCalibrationPoints:calibrationPoints anchorPointIndex:0];
    [_rectangles addObject:rect];
    
    calibrationPoints = [NSMutableArray new];
    [calibrationPoints addObject:_points[7]];
    [calibrationPoints addObject:_points[8]];
    [calibrationPoints addObject:_points[5]];
    [calibrationPoints addObject:_points[6]];
    rect = [[CalibrationRectangle alloc] initWithCalibrationPoints:calibrationPoints anchorPointIndex:1];
    [_rectangles addObject:rect];
}

- (void)_processLoadedData
{
    [_rectangles removeAllObjects];
    
    CGFloat centerX = _points[0].calibrationCoords.x + (_points[2].calibrationCoords.x - _points[0].calibrationCoords.x) * 0.5 + _points[7].calibrationCoords.x + (_points[3].calibrationCoords.x - _points[7].calibrationCoords.x) * 0.5 + _points[6].calibrationCoords.x + (_points[4].calibrationCoords.x - _points[6].calibrationCoords.x) * 0.5 + _points[1].calibrationCoords.x + _points[8].calibrationCoords.x + _points[5].calibrationCoords.x;
    centerX /= 6.0;
    
    CGFloat centerY = _points[2].calibrationCoords.y + (_points[4].calibrationCoords.y - _points[2].calibrationCoords.y) * 0.5 + _points[1].calibrationCoords.y + (_points[5].calibrationCoords.y - _points[1].calibrationCoords.y) * 0.5 + _points[0].calibrationCoords.y + (_points[6].calibrationCoords.y - _points[0].calibrationCoords.y) * 0.5 + _points[7].calibrationCoords.y + _points[8].calibrationCoords.y + _points[3].calibrationCoords.y;
    centerY /= 6.0;
    
    CGFloat width = (_points[2].calibrationCoords.x - _points[0].calibrationCoords.x) + (_points[3].calibrationCoords.x - _points[7].calibrationCoords.x) + (_points[4].calibrationCoords.x - _points[6].calibrationCoords.x);
    width /= 3.0;
    
    CGFloat height = (_points[4].calibrationCoords.y - _points[2].calibrationCoords.y) + (_points[5].calibrationCoords.y - _points[1].calibrationCoords.y) + (_points[6].calibrationCoords.y - _points[0].calibrationCoords.y);
    height /= 3.0;
    
    _points[0].calibrationCoords = CGPointMake(centerX - width * 0.5, centerY - height * 0.5);
    _points[1].calibrationCoords = CGPointMake(centerX, centerY - height * 0.5);
    _points[2].calibrationCoords = CGPointMake(centerX + width * 0.5, centerY - height * 0.5);
    _points[7].calibrationCoords = CGPointMake(centerX - width * 0.5, centerY);
    _points[8].calibrationCoords = CGPointMake(centerX, centerY);
    _points[3].calibrationCoords = CGPointMake(centerX + width * 0.5, centerY);
    _points[6].calibrationCoords = CGPointMake(centerX - width * 0.5, centerY + height * 0.5);
    _points[5].calibrationCoords = CGPointMake(centerX, centerY + height * 0.5);
    _points[4].calibrationCoords = CGPointMake(centerX + width * 0.5, centerY + height * 0.5);
    
    NSMutableArray* calibrationPoints = [NSMutableArray new];
    [calibrationPoints addObject:_points[0]];
    [calibrationPoints addObject:_points[1]];
    [calibrationPoints addObject:_points[8]];
    [calibrationPoints addObject:_points[7]];
    CalibrationRectangle* rect = [[CalibrationRectangle alloc] initWithCalibrationPoints:calibrationPoints anchorPointIndex:2];
    [_rectangles addObject:rect];
    
    calibrationPoints = [NSMutableArray new];
    [calibrationPoints addObject:_points[1]];
    [calibrationPoints addObject:_points[2]];
    [calibrationPoints addObject:_points[3]];
    [calibrationPoints addObject:_points[8]];
    rect = [[CalibrationRectangle alloc] initWithCalibrationPoints:calibrationPoints anchorPointIndex:3];
    [_rectangles addObject:rect];
    
    calibrationPoints = [NSMutableArray new];
    [calibrationPoints addObject:_points[8]];
    [calibrationPoints addObject:_points[3]];
    [calibrationPoints addObject:_points[4]];
    [calibrationPoints addObject:_points[5]];
    rect = [[CalibrationRectangle alloc] initWithCalibrationPoints:calibrationPoints anchorPointIndex:0];
    [_rectangles addObject:rect];
    
    calibrationPoints = [NSMutableArray new];
    [calibrationPoints addObject:_points[7]];
    [calibrationPoints addObject:_points[8]];
    [calibrationPoints addObject:_points[5]];
    [calibrationPoints addObject:_points[6]];
    rect = [[CalibrationRectangle alloc] initWithCalibrationPoints:calibrationPoints anchorPointIndex:1];
    [_rectangles addObject:rect];
}

- (void)save
{
    NSMutableArray* toSave = [NSMutableArray new];
    for (CalibrationPoint* p in self.points)
    {
        [toSave addObject:@{@"screen" : NSStringFromCGPoint(p.screenCoords), @"calibrated" : NSStringFromCGPoint(p.calibrationCoords)}];
    }
    
    NSFileManager* fileManager = [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath:[CalibrationResult _calibrationDirectoryURL].path])
    {
        NSError* err = nil;
        BOOL result = [fileManager createDirectoryAtURL:[CalibrationResult _calibrationDirectoryURL]
                            withIntermediateDirectories:NO
                                             attributes:nil
                                                  error:&err];
        NSLog(@"%d", result);
    }
    
    [toSave writeToURL:[CalibrationResult _calibrationFileURL] atomically:YES];
}

+ (NSURL*)_applicationDocumentsDirectoryURL
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

+ (NSURL*)_calibrationDirectoryURL
{
    return [[CalibrationResult _applicationDocumentsDirectoryURL] URLByAppendingPathComponent:@"Calibration"];
}

+ (NSURL*)_calibrationFileURL
{
    return [[CalibrationResult _calibrationDirectoryURL] URLByAppendingPathComponent:@"result.plist"];
}
@end
