//
//  ViewController.h
//  Marker
//
//  Created by Vitaliy Kolomoets on 3/24/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarkerView.h"

@interface ViewController : UIViewController

@property (weak) IBOutlet UITextView*       logTextView;
@property (weak) IBOutlet UITableView*      devicesListTableView;
@property (weak) IBOutlet UIButton*         disconnectButton;
@property (weak) IBOutlet UIButton*         recalibrateButton;
@property (weak) IBOutlet MarkerView*       markerView;
@end

