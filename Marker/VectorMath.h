//
//  VectorMath.h
//  Marker
//
//  Created by Vitaliy Kolomoets on 8/16/16.
//  Copyright © 2016 bMuse. All rights reserved.
//


#import <UIKit/UIKit.h>

static inline CGFloat projectVectorOnVector(CGVector vectorToBeProjected, CGVector vectorToProjectOn)
{
    return (vectorToBeProjected.dx * vectorToProjectOn.dx + vectorToBeProjected.dy * vectorToProjectOn.dy) / sqrt(vectorToProjectOn.dx * vectorToProjectOn.dx + vectorToProjectOn.dy * vectorToProjectOn.dy);
}

static inline CGFloat CGVectorGetLength(CGVector vector)
{
    return sqrt(vector.dx * vector.dx + vector.dy * vector.dy);
}

