//
//  AppDelegate.h
//  Marker
//
//  Created by Vitaliy Kolomoets on 3/24/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

