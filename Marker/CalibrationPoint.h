//
//  CalibrationPoint.h
//  Marker
//
//  Created by Vitaliy Kolomoets on 8/16/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CalibrationLogic.h"

@interface CalibrationPoint : NSObject

@property   CGPoint         screenCoords;
@property   CGPoint         calibrationCoords;

@property   BOOL            calibrated;

- (void)pointValueReceived:(CGPoint)markerPoint;

@end
