//
//  CalibrationResult.h
//  Marker
//
//  Created by Vitaliy Kolomoets on 8/16/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CalibrationPoint.h"

//#define NINE_POINTS_CALIBRATION

@interface CalibrationResult : NSObject

@property NSMutableArray<CalibrationPoint*>*       points;

+ (instancetype)savedResult;
+ (void)clear;

- (CGPoint)screenPointForMarkerPoint:(CGPoint)markerPoint;
- (void)processData;
- (void)save;


@end
