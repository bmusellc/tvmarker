
uniform sampler2D texture;
uniform lowp vec2 brushesAtlasSingeItemSize;

varying lowp vec4 color;
varying lowp vec2 brushIndexesToUse;

void main()
{
    //lowp vec2 texCoord = (gl_PointCoord + brushIndexesToUse) * brushesAtlasSingeItemSize;
    lowp vec2 texCoord = gl_PointCoord;
	gl_FragColor = color * texture2D(texture, texCoord);
}
