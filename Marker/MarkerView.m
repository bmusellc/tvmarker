//
//  MarkerView.m
//  Marker
//
//  Created by Vitaliy Kolomoets on 3/28/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import "MarkerView.h"
#import "MarkerTouch.h"
#import "FancyDrawingView.h"


#define DISPLAY_RETICULE
#define DISPLAY_GRID

@implementation MarkerView
{
    FancyDrawingView*       _drawingView;
    CGPoint                 _markerPosition;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
//        _calibrationHintLabel.hidden = YES;
        
        _drawingView = [[FancyDrawingView alloc]initWithFrame:self.bounds];
        [self insertSubview:_drawingView atIndex:0];
        
        _reticule = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cross"]];
        _reticule.frame = CGRectMake(0, 0, 100, 100);
        [self addSubview:_reticule];
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
//    _drawingView.frame = self.bounds;
    [_drawingView setNeedsLayout];
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
}

- (void)setTouch:(MarkerTouch *)touch
{
    _touch = touch;
    
    if(CGPointEqualToPoint(_markerPosition, _touch.currentPosition))
        return;
    
    
    _reticule.center = _touch.currentPosition;
    
    if(touch.state == MarkerStateActive)
    {
        if(!CGPointEqualToPoint(_markerPosition, CGPointZero))
        {
//            _touch.currentPosition  = CGPointMake(_markerPosition.x, self.bounds.size.height - _markerPosition.y);
            _touch.prevPosition     = _markerPosition;//CGPointMake(_markerPosition.x, self.bounds.size.height - _markerPosition.y);
            
            [_drawingView processMarkerTouch:touch];
            _markerPosition = _touch.currentPosition;
        }
        else
        {
            _markerPosition = _touch.currentPosition;
        }
    }
    
    [self setNeedsDisplay];
}

- (void)setCalibrationPoint:(CalibrationPoint*)point
{
    _reticule.center = point.screenCoords;
}


-(void)setMarkerIsActive:(BOOL)markerIsActive
{
    if(_markerIsActive == markerIsActive)
    {
        return;
    }
    _markerIsActive = markerIsActive;
    
    if(!_markerIsActive)
    {
        _markerPosition = CGPointZero;
        [_drawingView clearPointsHistory];
    }
}

- (void)cleanUp
{
    [self setNeedsDisplay];
}

- (void)drawGrid
{
#ifdef DISPLAY_GRID
    int verticalLines = 16;
    int horizontalLines = 9;
    
    CGFloat verticalOffset = self.bounds.size.width / verticalLines;
    CGFloat horizontalOffset = self.bounds.size.height / horizontalLines;
    
    for (int i = 1; i < verticalLines; i++)
    {
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(verticalOffset * i - 1,
                                                                0,
                                                                2,
                                                                self.bounds.size.height)];
        line.backgroundColor = [UIColor blackColor];
        [self addSubview:line];
    }
    
    for (int i = 1; i < horizontalLines; i++)
    {
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                horizontalOffset * i - 1,
                                                                self.bounds.size.width,
                                                                2)];
        line.backgroundColor = [UIColor blackColor];
        [self addSubview:line];
    }
#endif
}

@end
