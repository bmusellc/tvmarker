//
//  main.m
//  Marker
//
//  Created by Vitaliy Kolomoets on 3/24/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
