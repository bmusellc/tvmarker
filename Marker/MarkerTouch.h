//
//  MarkerTouch.h
//  Marker
//
//  Created by Shon Alien on 4/4/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum
{
    MarkerStateIdle = 0,
    MarkerStateActive = 1,
} MarkerState;

@interface MarkerTouch : NSObject

@property MarkerState   state;
@property CGPoint       prevPosition;
@property CGPoint       currentPosition;
@property NSDate*       timestamp;
@property UIColor*      color;

- (instancetype) initWithCurrentPosition:(CGPoint)curPosition prevPosition:(CGPoint)prevPosition color:(UIColor *)color;

@end
