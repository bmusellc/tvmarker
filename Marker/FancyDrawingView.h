//
//  FancyDrawingView.h
//  Sessions
//
//  Created by developer on 8/29/15.
//  Copyright © 2015 TouchCast. All rights reserved.
//

@import UIKit;
@import Foundation;

@class FancyDrawingView;
@class MarkerTouch;

@protocol FancyDrawingViewDelegate<NSObject>

@optional

- (void)drawingViewDidStartDrawingOperation:(FancyDrawingView*)sender;
- (void)drawingViewDidFinishDrawingOperation:(FancyDrawingView*)sender;

//super hand methods
- (float)drawingViewStartDrawingDelay:(FancyDrawingView*)sender;
- (void)drawingViewShouldShowHand:(FancyDrawingView*)sender atPosition:(CGPoint)point;
- (void)drawingView:(FancyDrawingView*)sender moveHandToPoint:(CGPoint)point animationDuration:(CGFloat)animationDuration;
- (void)drawingViewShouldHideHand:(FancyDrawingView*)sender;

//drawing sync helper methods
- (CGRect)drawingViewFrameOnCanvas:(FancyDrawingView*)sender;
- (void)drawingViewRequestsMovingToFrameOnCanvas:(CGRect)frame internalBounds:(CGRect)internalBounds;

@end

typedef NS_ENUM(NSUInteger, FancyDrawingViewBrushType)
{
    FancyDrawingViewBrushTypeInkPen,
    FancyDrawingViewBrushTypeMax
};

@interface FancyDrawingView : UIView

@property (weak)        id<FancyDrawingViewDelegate>    delegate;
@property (assign)      BOOL                            processFingerTouchAsDrawing;

@property (readonly)    UIImage*                        drawing;
@property (readonly)    CGRect                          rectActuallyUsedOnDrawing;

@property(readonly)     BOOL                            animatedDrawingIsInProgress;

@property(assign)       FancyDrawingViewBrushType       brushType;
@property(readonly)     BOOL                            shouldSendDrawingCommands;


- (instancetype)initWithFrame:(CGRect)frame;

- (void)processMarkerTouch:(MarkerTouch*)touch;
- (void)processMarkerPoints:(NSArray<NSValue *> *)points;

- (void)clearPointsHistory;
- (void)clear;

//- (void)setBrushColorWithRed:(CGFloat)red
//                        green:(CGFloat)green
//                        blue:(CGFloat)blue;


@end
