//
//  CalibrationRectangle.m
//  Marker
//
//  Created by Vitaliy Kolomoets on 8/16/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import "CalibrationRectangle.h"
#import "VectorMath.h"

@implementation CalibrationRectangle
{
    NSUInteger      _upIndex;
    NSUInteger      _sideIndex;
    CGSize          _sizeOnScreen;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _calibrationPoints = [NSMutableArray new];
    }
    return self;
}

-(instancetype)initWithCalibrationPoints:(NSMutableArray<CalibrationPoint*>*)points anchorPointIndex:(NSUInteger)index
{
    self = [super init];
    if (self)
    {
        _calibrationPoints = [NSMutableArray arrayWithArray:points];
        _anchorPointIndex = index;
        _rectangle = CGRectMake(points[0].calibrationCoords.x,
                                points[0].calibrationCoords.y,
                                points[2].calibrationCoords.x - points[0].calibrationCoords.x,
                                points[2].calibrationCoords.y - points[0].calibrationCoords.y);
        
        _sizeOnScreen = CGSizeMake(points[2].screenCoords.x - points[0].screenCoords.x, points[2].screenCoords.y - points[0].screenCoords.y);
        
        if(_anchorPointIndex % 2)
        {
            _upIndex = (_anchorPointIndex + 1) % 4;
            _sideIndex = (_anchorPointIndex + 3) % 4;
        }
        else
        {
            _upIndex = (_anchorPointIndex + 3) % 4;
            _sideIndex = (_anchorPointIndex + 1) % 4;
        }
        
        _upVector = CGVectorMake(_calibrationPoints[_upIndex].calibrationCoords.x - _calibrationPoints[_anchorPointIndex].calibrationCoords.x,
                                 _calibrationPoints[_upIndex].calibrationCoords.y -_calibrationPoints[_anchorPointIndex].calibrationCoords.y);
        _sideVector = CGVectorMake(_calibrationPoints[_sideIndex].calibrationCoords.x - _calibrationPoints[_anchorPointIndex].calibrationCoords.x,
                                   _calibrationPoints[_sideIndex].calibrationCoords.y - _calibrationPoints[_anchorPointIndex].calibrationCoords.y);
    }
    return self;
}

-(CalibrationPoint *)anchorPoint
{
    return _calibrationPoints[_anchorPointIndex];
}

- (CGPoint)calculateScreenCoordinatedForMarkerPoint:(CGPoint)markerPoint
{
    CGPoint result;
    
    CGVector pVector = CGVectorMake(markerPoint.x - self.anchorPoint.calibrationCoords.x, markerPoint.y - self.anchorPoint.calibrationCoords.y);
    CGFloat sideProjAbs = projectVectorOnVector(pVector, self.sideVector);
    CGFloat upProjAbs = projectVectorOnVector(pVector, self.upVector);
    CGFloat sideProjRel = sideProjAbs / CGVectorGetLength(self.sideVector);
    CGFloat upProjRel = upProjAbs / CGVectorGetLength(self.upVector);
    
    CGFloat upSign = self.calibrationPoints[_anchorPointIndex].screenCoords.y > self.calibrationPoints[_upIndex].screenCoords.y ? -1.0 : 1.0;
    CGFloat sideSign = self.calibrationPoints[_anchorPointIndex].screenCoords.x > self.calibrationPoints[_sideIndex].screenCoords.x ? -1.0 : 1.0;
    
    result.x = self.anchorPoint.screenCoords.x + sideProjRel * _sizeOnScreen.width * sideSign;
    result.y = self.anchorPoint.screenCoords.y + upProjRel * _sizeOnScreen.height * upSign;
    NSLog(@"%.2f %.2f", result.x, result.y);
    return result;
}

@end
