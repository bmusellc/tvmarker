//
//  CalibrationRectangle.h
//  Marker
//
//  Created by Vitaliy Kolomoets on 8/16/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CalibrationPoint.h"

@interface CalibrationRectangle : NSObject

@property   CGRect                              rectangle;
@property   NSMutableArray<CalibrationPoint*>*  calibrationPoints;
@property   NSUInteger                          anchorPointIndex;
@property   (readonly) CalibrationPoint*        anchorPoint;
@property   CGVector                            upVector;
@property   CGVector                            sideVector;

- (instancetype)initWithCalibrationPoints:(NSMutableArray<CalibrationPoint*>*)points anchorPointIndex:(NSUInteger)index;
- (CGPoint)calculateScreenCoordinatedForMarkerPoint:(CGPoint)point;
@end
