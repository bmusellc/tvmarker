//
//  MarkerTouch.m
//  Marker
//
//  Created by Shon Alien on 4/4/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import "MarkerTouch.h"

@implementation MarkerTouch

- (instancetype)initWithCurrentPosition:(CGPoint)curPosition prevPosition:(CGPoint)prevPosition color:(UIColor *)color
{
    self = [self init];
    
    if (self) {
        self.currentPosition   = curPosition;
        self.prevPosition      = prevPosition;
        self.color             = color;
    }
    
    return self;
}

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        self.timestamp         = [NSDate date];
    }
    
    return self;
}

@end
