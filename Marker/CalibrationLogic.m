//
//  CalibrationLogic.m
//  Marker
//
//  Created by Vitaliy Kolomoets on 8/16/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import "CalibrationLogic.h"
#import "CalibrationPoint.h"
#import "CalibrationResult.h"


@implementation CalibrationLogic
{
    NSMutableArray<CalibrationPoint*>*     _calibrationPoints;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _calibrationResult = [CalibrationResult savedResult];
        _calibrationInsets = UIEdgeInsetsMake(0.222, 0, 0, 0);
        if(_calibrationResult)
        {
            _state = CalibrationLogicStateCalibrated;
        }
        else
        {
            _calibrationResult = [CalibrationResult new];
            _state = CalibrationLogicStateInited;
            _calibrationPoints = [NSMutableArray new];
            CGSize screenSize = [UIScreen mainScreen].bounds.size;
            
            CGSize calibrationSize = CGSizeMake(screenSize.width * (1 - _calibrationInsets.left - _calibrationInsets.right), screenSize.height * (1 - _calibrationInsets.top - _calibrationInsets.bottom));
            
            CalibrationPoint* ulPoint = [CalibrationPoint new];//UL
            ulPoint.screenCoords = CGPointMake(_calibrationInsets.left * screenSize.width, _calibrationInsets.top * screenSize.height);
            [_calibrationPoints addObject:ulPoint];

#ifdef NINE_POINTS_CALIBRATION
            CalibrationPoint* ucPoint = [CalibrationPoint new];//UC
            ucPoint.screenCoords = CGPointMake(screenSize.width * _calibrationInsets.left + calibrationSize.width * 0.5, _calibrationInsets.top * screenSize.height);
            [_calibrationPoints addObject:ucPoint];
#endif
            
            CalibrationPoint* urPoint = [CalibrationPoint new];//UR
            urPoint.screenCoords = CGPointMake(screenSize.width * (1.0 - _calibrationInsets.right), screenSize.height * _calibrationInsets.top);
            [_calibrationPoints addObject:urPoint];

#ifdef NINE_POINTS_CALIBRATION
            CalibrationPoint* crPoint = [CalibrationPoint new];//CR
            crPoint.screenCoords = CGPointMake(screenSize.width * (1.0 - _calibrationInsets.right), screenSize.height * _calibrationInsets.top + calibrationSize.height * 0.5);
            [_calibrationPoints addObject:crPoint];
#endif
            CalibrationPoint* brPoint = [CalibrationPoint new];//BR
            brPoint.screenCoords = CGPointMake(screenSize.width * (1.0 - _calibrationInsets.right), screenSize.height * (1.0 - _calibrationInsets.bottom));
            [_calibrationPoints addObject:brPoint];
      
#ifdef NINE_POINTS_CALIBRATION
            CalibrationPoint* bcPoint = [CalibrationPoint new];//BC
            bcPoint.screenCoords = CGPointMake(screenSize.width * _calibrationInsets.left + calibrationSize.width * 0.5, screenSize.height * (1.0 - _calibrationInsets.bottom));
            [_calibrationPoints addObject:bcPoint];
#endif
            
            CalibrationPoint* blPoint = [CalibrationPoint new];//BL
            blPoint.screenCoords = CGPointMake(_calibrationInsets.left * screenSize.width, screenSize.height * (1.0 - _calibrationInsets.bottom));
            [_calibrationPoints addObject:blPoint];
      
#ifdef NINE_POINTS_CALIBRATION
            CalibrationPoint* clPoint = [CalibrationPoint new];//CL
            clPoint.screenCoords = CGPointMake(_calibrationInsets.left * screenSize.width, screenSize.height * _calibrationInsets.top + calibrationSize.height * 0.5);
            [_calibrationPoints addObject:clPoint];
            
            CalibrationPoint* cPoint = [CalibrationPoint new];//C
            cPoint.screenCoords = CGPointMake(screenSize.width * _calibrationInsets.left + calibrationSize.width * 0.5, screenSize.height * _calibrationInsets.top +  calibrationSize.height * 0.5);
            [_calibrationPoints addObject:cPoint];
#endif
        }
    }
    return self;
}

- (void)startCalibration
{
    self.state = CalibrationLogicStateCalibratingUL;
}

-(void)onMarkerTouchReceived:(MarkerTouch *)touch
{
    if(_state == CalibrationLogicStateInited || _state == CalibrationLogicStateCalibrated)
    {
        return;
    }
    
    [self.currentCalibrationPoint pointValueReceived:touch.currentPosition];
    
    if(self.currentCalibrationPoint.calibrated)
    {
        self.state = _state + 1;
    }
}

-(void)setState:(CalibrationLogicState)state
{
    CalibrationLogicState oldState = _state;
    _state = state;
    switch (_state)
    {
        case CalibrationLogicStateInited:
            break;
            
        case CalibrationLogicStateCalibratingUL:
        {
            self.currentCalibrationPoint = _calibrationPoints[0];
        }
            break;
            
        case CalibrationLogicStateCalibrated:
        {
            [_calibrationResult.points addObject:self.currentCalibrationPoint];
            [_calibrationResult processData];
            [_calibrationResult save];
            self.currentCalibrationPoint = nil;
        }
            break;
            
        default:
        {
            [_calibrationResult.points addObject:self.currentCalibrationPoint];
            self.currentCalibrationPoint = _calibrationPoints[_state - 1];
        }
            break;
    }
    
    [_delegate onCalibrationLogic:self stateDidChangeFromState:oldState];
}
@end
