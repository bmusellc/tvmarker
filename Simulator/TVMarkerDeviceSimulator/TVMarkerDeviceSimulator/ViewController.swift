//
//  ViewController.swift
//  BluetoothHost
//
//  Created by Yuriy Belozerov on 1/27/16.
//  Copyright © 2016 Yuriy Belozerov. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController, CBPeripheralManagerDelegate {

    @IBOutlet weak var simulationView: SimulationView!
	var manager: CBPeripheralManager?
	var characteristic: CBMutableCharacteristic?
	var service: CBMutableService?
	
    var st: UInt8  = 4
	var x1: UInt16 = 0
	var y1: UInt16 = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.manager = CBPeripheralManager(delegate: self, queue: nil, options: nil)
	}

	func addService() {
		self.characteristic = CBMutableCharacteristic(type: CBUUID(string: "FF01"), properties: .notify, value: nil, permissions: .readable)
		self.service = CBMutableService(type: CBUUID(string: "E36AE8E3-33F8-485E-AA70-2B4ACE7C9CE5"), primary: true)
		self.service?.characteristics = [self.characteristic!]
		self.manager?.add(self.service!)
	}
	
	func startAdvertising () {
		self.manager?.startAdvertising([CBAdvertisementDataServiceUUIDsKey : [self.service!.uuid], CBAdvertisementDataLocalNameKey : "SmartphoneGun"])
	}
	
	func sendData()
    {
        x1 = UInt16(round(simulationView.position.x))
        y1 = UInt16(round(simulationView.bounds.size.height - simulationView.position.y))
        let i0 = (simulationView.active << 6) | (simulationView.selectedColor)
		let i1 = x1 >> 8
		let i2 = x1 & 0xFF
		let i3 = y1 >> 8
		let i4 = y1 & 0xFF
		
		let data = Data(bytes: UnsafePointer<UInt8>([UInt8(i0), UInt8(i1), UInt8(i2), UInt8(i3), UInt8(i4)] as [UInt8]), count: 5)
		let result = self.manager?.updateValue(data, for: self.characteristic!, onSubscribedCentrals: nil)
		if result == false
        {
//			print("!!!")
		}
        print(y1);
	}
	
	func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
		switch peripheral.state {
		case .unknown:
			print("Unknown")
		case .poweredOn:
			print("Powered On")
			addService()
		default:
			print("Other")
		}
	}
	
	func peripheralManager(_ peripheral: CBPeripheralManager, didAdd service: CBService, error: Error?) {
		if error == nil {
			print("Service Added")
			startAdvertising()
		}
		else {
			print(error)
		}
	}
	
	func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
		if error == nil {
			print("Advertising Started")

		}
		else {
			print(error)
		}
	}
	
	func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveRead request: CBATTRequest) {
		print("Read Request Received")
	}
	
	func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didSubscribeTo characteristic: CBCharacteristic) {
		print("Central Did Subscribed: " + characteristic.uuid.uuidString)
		Timer.scheduledTimer(timeInterval: 1 / 20.0, target: self, selector: #selector(ViewController.sendData), userInfo: nil, repeats: true)
	}
}

