//
//  CalibrationPoint.m
//  Marker
//
//  Created by Vitaliy Kolomoets on 8/16/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import "CalibrationPoint.h"

#define NUMBER_OF_POINTS_FOR_CALIBRATION       40
#define NUMBER_OF_POINTS_TO_IGNORE             10

@implementation CalibrationPoint
{
    NSUInteger      _numberOfReceivedPoints;
    CGPoint         _receivedPoints[NUMBER_OF_POINTS_FOR_CALIBRATION];
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (void)pointValueReceived:(CGPoint)markerPoint
{
    if(self.calibrated)
    {
        return;
    }
    
    _numberOfReceivedPoints++;
    
    if(_numberOfReceivedPoints <= NUMBER_OF_POINTS_TO_IGNORE)
    {
        return;
    }
    
    
    if(_numberOfReceivedPoints <= NUMBER_OF_POINTS_TO_IGNORE + NUMBER_OF_POINTS_FOR_CALIBRATION)
    {
        _receivedPoints[_numberOfReceivedPoints - NUMBER_OF_POINTS_TO_IGNORE - 1] = markerPoint;
    }
    else
    {
        
        for (int i = 0; i < NUMBER_OF_POINTS_FOR_CALIBRATION; i++)
        {
            _calibrationCoords.x += _receivedPoints[i].x;
            _calibrationCoords.y += _receivedPoints[i].y;
        }
        
        _calibrationCoords.x /= (CGFloat)NUMBER_OF_POINTS_FOR_CALIBRATION;
        _calibrationCoords.y /= (CGFloat)NUMBER_OF_POINTS_FOR_CALIBRATION;
        NSLog(@"calibrated x = %f, y = %f", _calibrationCoords.x, _calibrationCoords.y);
        self.calibrated = YES;
    }
    
}

@end
