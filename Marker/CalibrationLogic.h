//
//  CalibrationLogic.h
//  Marker
//
//  Created by Vitaliy Kolomoets on 8/16/16.
//  Copyright © 2016 bMuse. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "MarkerTouch.h"
@class CalibrationPoint, CalibrationLogic, CalibrationResult;

typedef enum {
    CalibrationLogicStateInited,
    CalibrationLogicStateCalibratingUL,//upper left
#ifdef NINE_POINTS_CALIBRATION
    CalibrationLogicStateCalibratingUC,//upper center
#endif
    CalibrationLogicStateCalibratingUR,//upper right
#ifdef NINE_POINTS_CALIBRATION
    CalibrationLogicStateCalibratingCR,//center right
#endif
    CalibrationLogicStateCalibratingBR,//bottom right
#ifdef NINE_POINTS_CALIBRATION
    CalibrationLogicStateCalibratingBC,//bottom center
#endif
    CalibrationLogicStateCalibratingBL,//bottom left
#ifdef NINE_POINTS_CALIBRATION
    CalibrationLogicStateCalibratingCL,//center left
    CalibrationLogicStateCalibratingC,//center
#endif
    CalibrationLogicStateCalibrated
}CalibrationLogicState;

@protocol CalibrationLogicDelegate <NSObject>
- (void)onCalibrationLogic:(CalibrationLogic*)cLogic stateDidChangeFromState:(CalibrationLogicState)oldState;
@end

@interface CalibrationLogic : NSObject

@property (nonatomic)   CalibrationLogicState               state;
@property               CalibrationPoint*                   currentCalibrationPoint;
@property               UIEdgeInsets                        calibrationInsets;//relative values
@property               CalibrationResult*                  calibrationResult;
@property (weak)        NSObject<CalibrationLogicDelegate>* delegate;

- (void)startCalibration;

- (void)onMarkerTouchReceived:(MarkerTouch*)touch;

@end
