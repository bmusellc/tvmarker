//
//  SimulationView.swift
//  BluetoothHost
//
//  Created by Vitaliy Kolomoets on 3/25/16.
//  Copyright © 2016 Yuriy Belozerov. All rights reserved.
//

import UIKit


class SimulationView: UIView
{
    @IBOutlet weak var colorPicker: UISegmentedControl!
    var selectedColor       = 0
    var active              = 0
    var position:CGPoint    = CGPoint(x: 0, y: 0)
    var touchesArray:NSMutableArray
    required init?(coder aDecoder: NSCoder)
    {
        touchesArray = NSMutableArray()
        super.init(coder: aDecoder)
        self.isMultipleTouchEnabled = false
    }

    @IBAction func onColorPicked(sender: UISegmentedControl)
    {
        selectedColor = sender.selectedSegmentIndex + 1;
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if(active > 0)
        {
            return;
        }
        active = 1
        position = touches.first!.location(in: self)
        let touchDict = NSMutableDictionary()
        touchDict["pos"] = NSStringFromCGPoint(CGPoint(x: position.x / self.bounds.size.width, y: position.y / self.bounds.size.height))
        touchDict["time"] = NSNumber(value: Date().timeIntervalSinceReferenceDate as Double)
        touchesArray.add(touchDict)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        position = touches.first!.location(in: self)
        let touchDict = NSMutableDictionary()
        touchDict["pos"] = NSStringFromCGPoint(CGPoint(x: position.x / self.bounds.size.width, y: position.y / self.bounds.size.height))
        touchDict["time"] = NSNumber(value: Date().timeIntervalSinceReferenceDate as Double)
        touchesArray.add(touchDict)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        position = touches.first!.location(in: self)
        let touchDict = NSMutableDictionary()
        touchDict["pos"] = NSStringFromCGPoint(CGPoint(x: position.x / self.bounds.size.width, y: position.y / self.bounds.size.height))
        touchDict["time"] = NSNumber(value: Date().timeIntervalSinceReferenceDate as Double)
        touchesArray.add(touchDict)
        active = 0
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        touchesArray.write(toFile: documentsPath + "/marker.txt", atomically: true)
        /*/Users/vitaliykolomoets/projects*/
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        active = 0
    }
}
