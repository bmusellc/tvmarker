//
//  Shaders.metal
//  AlphaBlending
//
//  Created by Warren Moore on 12/4/14.
//  Copyright (c) 2014 Metal By Example. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

struct Vertex
{
    packed_float4 position [[attribute(0)]];
    packed_float4 diffuseColor [[attribute(2)]];
};

struct ProjectedVertex
{
    float4 position [[position]];
    float4 diffuseColor [[user(diffuse_color)]];
};

struct Uniforms
{
    float4x4 viewProjectionMatrix;
};

struct InstanceUniforms
{
    float4x4 modelMatrix;
    float4x4 normalMatrix;
};

vertex ProjectedVertex project_vertex(constant Vertex *vertices [[buffer(0)]],
                                      constant Uniforms &uniforms [[buffer(1)]],
                                      constant InstanceUniforms *instanceUniforms [[buffer(2)]],
                                      uint vid [[vertex_id]])
{
    float4x4 modelMatrix = instanceUniforms->modelMatrix;

    ProjectedVertex outVert;
    outVert.position = uniforms.viewProjectionMatrix * modelMatrix * float4(vertices[vid].position);
    outVert.diffuseColor = vertices[vid].diffuseColor;
    return outVert;
}

fragment float4 texture_fragment(ProjectedVertex vert [[stage_in]])
{
    return vert.diffuseColor;
}


